<?php 
	
	if(!isset($contacts))
	{
		$contacts = $this->site_model->get_contacts();
	}
	$data['contacts'] = $contacts; 

?>
<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <?php echo $this->load->view('site/includes/header', '', TRUE); ?>
</head>

<body>
    <!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

    <!-- Main Wrapper Start Here -->
    <div class="wrapper white-bg">
        <!-- Main Header Area Start Here -->
          <!-- Newsletter Popup Start -->
            <input type="hidden" id="base_url" value="<?php echo base_url()?>">
            <input type="hidden" id="site_url" value="<?php echo site_url()?>">
    	
            <!-- Top Navigation -->
            <?php echo $this->load->view('site/includes/top_navigation', $data, TRUE); ?>
        <!-- Main Header Area End Here -->
        
        <!-- LogIn Page Start -->
        <div class="log-in ptb-45">
            <div class="container">
            	<?php
			       $success_message = $this->session->userdata('front_success_message');
			       $error_message = $this->session->userdata('front_error_message');
			                    
			        if(!empty($success_message))
			        {
			            echo '<div class="alert alert-success"><p>'.$success_message.'</p></div>';
			            $this->session->unset_userdata('success_message');
			        }

			        if(!empty($error_message))
			        {
			            echo '<div class="alert alert-danger"><p>'.$error_message.'</p></div>';
			            $this->session->unset_userdata('error_message');
			        }
			      ?>
                <div class="row">
                    <!-- New Customer Start -->
                    <div class="col-md-6">
                        <div class="well mb-sm-30">
                            <div class="new-customer">
                                <h3 class="custom-title">new customer</h3>
                                <p class="mtb-10"><strong>Sign up for an account</strong></p>
                                <!-- <p>By creating an account you will be able to shop faster</p> -->
                                <div class="row">
				                    <div class="col-sm-12">
				                         <?php 
					                          echo form_open_multipart(base_url().'site/account/update_account',array("class" => "form-register", "role" => "form","id"=>"login_form"));
					                        ?>
				                            <fieldset>
				                                <div class="form-group d-md-flex align-items-md-center">
				                                    <label class="control-label col-md-4" for="f-name"><span class="require">*</span>First Name</label>
				                                    <div class="col-md-8">
				                                        <input type="text" class="form-control" id="f-name" name="first_name" placeholder="First Name">
				                                    </div>
				                                </div>
				                                <div class="form-group d-md-flex align-items-md-center">
				                                    <label class="control-label col-md-4" for="l-name"><span class="require">*</span>Last Name</label>
				                                    <div class="col-md-8">
				                                        <input type="text" class="form-control" id="l-name" name="last_name" placeholder="Last Name">
				                                    </div>
				                                </div>
				                                <div class="form-group d-md-flex align-items-md-center">
				                                    <label class="control-label col-md-4" for="email"><span class="require">*</span>Enter you email address here...</label>
				                                    <div class="col-md-8">
				                                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter you email address here...">
				                                    </div>
				                                </div>
				                                <div class="form-group d-md-flex align-items-md-center">
				                                    <label class="control-label col-md-4" for="number"><span class="require">*</span>Telephone</label>
				                                    <div class="col-md-8">
				                                        <input type="email" class="form-control" id="number" name="phone" placeholder="Telephone">
				                                    </div>
				                                </div>
				                            </fieldset>
				                            <fieldset>
				                                <legend>Your Password</legend>
				                                <div class="form-group d-md-flex align-items-md-center">
				                                    <label class="control-label col-md-4" for="pwd"><span class="require">*</span>Password:</label>
				                                    <div class="col-md-8">
				                                        <input type="password" class="form-control" id="pwd" name="password" placeholder="Password">
				                                    </div>
				                                </div>
				                                <div class="form-group d-md-flex align-items-md-center">
				                                    <label class="control-label col-md-4" for="pwd-confirm"><span class="require">*</span>Confirm Password</label>
				                                    <div class="col-md-8">
				                                        <input type="password" class="form-control" id="pwd-confirm" name="confirm_password" placeholder="Confirm password">
				                                    </div>
				                                </div>
				                            </fieldset>
				                            <fieldset class="newsletter-input">
				                                <legend>Newsletter</legend>
				                                <div class="form-group d-md-flex align-items-md-center">
				                                    <label class="col-md-4 control-label">Subscribe</label>
				                                    <div class="col-md-8 radio-button">
				                                         <label class="radio-inline"><input type="radio" name="optradio">Yes</label>
				                                         <label class="radio-inline"><input type="radio" name="optradio">No</label>
				                                    </div>
				                                </div>
				                            </fieldset>
				                            <div class="terms">
				                                <div class="float-md-right">
				                                    <span>I have read and agree to the <a href="#" class="agree"><b>Privacy Policy</b></a></span>
				                                    <input type="checkbox" name="agree" value="1"> &nbsp;
				                                    <input type="submit" value="Continue" class="return-customer-btn">
				                                </div>
				                            </div>
				                        </form>
				                    </div>
				                </div>

                            </div>

                              <!-- Row End -->
                
                <!-- Row End -->
                        </div>
                    </div>
                    <!-- New Customer End -->
                    <!-- Returning Customer Start -->
                    <div class="col-md-6">
                        <div class="well">
                            <div class="return-customer">
                                <h3 class="mb-10 custom-title">returning customer</h3>
                                <p class="mb-10"><strong>I already have an account</strong></p>
                                 <?php 
			                            echo form_open_multipart($this->uri->uri_string(),array("class" => "form", "role" => "form","id"=>"login_form"));
			                          ?>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" name="email" placeholder="Enter your email address..." id="input-email" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" name="password" placeholder="Password" id="input-password" class="form-control">
                                    </div>
                                    <p class="lost-password"><a href="#">Forgot password?</a></p>
                                    <input type="submit" value="Login" class="return-customer-btn">
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Returning Customer End -->
                </div>
                <!-- Row End -->
            </div>
            <!-- Container End -->
        </div>
        <!-- LogIn Page End -->
        
        <!-- Footer Area Start Here -->
        <?php echo $this->load->view('site/includes/footer', $data, TRUE); ?>
        <!-- Footer Area End Here -->
    </div>
    <!-- Main Wrapper End Here -->

    <!-- jquery 3.2.1 -->
    <script src="<?php echo base_url()."assets/themes/volga/";?>js/vendor/jquery-3.2.1.min.js"></script>
    <!-- Countdown js -->
    <script src="<?php echo base_url()."assets/themes/volga/";?>js/jquery.countdown.min.js"></script>
    <!-- Mobile menu js -->
    <script src="<?php echo base_url()."assets/themes/volga/";?>js/jquery.meanmenu.min.js"></script>
    <!-- ScrollUp js -->
    <script src="<?php echo base_url()."assets/themes/volga/";?>js/jquery.scrollUp.js"></script>
    <!-- Nivo slider js -->
    <script src="<?php echo base_url()."assets/themes/volga/";?>js/jquery.nivo.slider.js"></script>
    <!-- Fancybox js -->
    <script src="<?php echo base_url()."assets/themes/volga/";?>js/jquery.fancybox.min.js"></script>
    <!-- Jquery nice select js -->
    <script src="<?php echo base_url()."assets/themes/volga/";?>js/jquery.nice-select.min.js"></script>
    <!-- Jquery ui price slider js -->
    <script src="<?php echo base_url()."assets/themes/volga/";?>js/jquery-ui.min.js"></script>
    <!-- Owl carousel -->
    <script src="<?php echo base_url()."assets/themes/volga/";?>js/owl.carousel.min.js"></script>
    <!-- Bootstrap popper js -->
    <script src="<?php echo base_url()."assets/themes/volga/";?>js/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="<?php echo base_url()."assets/themes/volga/";?>js/bootstrap.min.js"></script>
    <!-- Plugin js -->
    <script src="<?php echo base_url()."assets/themes/volga/";?>js/plugins.js"></script>
    <!-- Main activaion js -->
    <script src="<?php echo base_url()."assets/themes/volga/";?>js/main.js"></script>
</body>

</html>