<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/site/controllers/site.php";

class Account extends site {
	
	function __construct()
	{
		parent:: __construct();
		
		$this->load->model('login/login_model');
		
		//user has logged in
		if($this->login_model->check_user_login())
		{

		}
		
		//user has not logged in
		else
		{
			$this->session->set_userdata('front_error_message', 'Please sign up/in to continue');
				
			redirect('user-login');
		}
	}
    
	/*
	*
	*	Open the account page
	*
	*/
	public function my_account()
	{
		//Required general page data
		$v_data['all_children'] = $this->categories_model->all_child_categories();
		$v_data['parent_categories'] = $this->categories_model->all_parent_categories();
		$v_data['crumbs'] = $this->site_model->get_crumbs();
		
		//page data
		$v_data['user_details'] = $this->users_model->get_user($this->session->userdata('user_id'));
		$data['content'] = $this->load->view('user/my_account', $v_data, true);
		
		$data['title'] = $this->site_model->display_page_title();
		$this->load->view('templates/general_page', $data);
	}
    
	/*
	*
	*	Open the orders list
	*
	*/
	public function orders_list()
	{
		//Required general page data
		$v_data['all_children'] = $this->categories_model->all_child_categories();
		$v_data['parent_categories'] = $this->categories_model->all_parent_categories();
		$v_data['crumbs'] = $this->site_model->get_crumbs();
		
		//page data
		$v_data['all_orders'] = $this->orders_model->get_user_orders($this->session->userdata('user_id'));
		$data['content'] = $this->load->view('user/orders_list', $v_data, true);
		
		$data['title'] = $this->site_model->display_page_title();
		$this->load->view('templates/general_page', $data);
	}
    
	/*
	*
	*	Open the user's details page
	*
	*/
	public function my_details()
	{
		//Required general page data
		$v_data['all_children'] = $this->categories_model->all_child_categories();
		$v_data['parent_categories'] = $this->categories_model->all_parent_categories();
		$v_data['crumbs'] = $this->site_model->get_crumbs();
		
		//page data
		$v_data['user_details'] = $this->users_model->get_user($this->session->userdata('user_id'));
		$data['content'] = $this->load->view('user/my_details', $v_data, true);
		
		$data['title'] = $this->site_model->display_page_title();
		$this->load->view('templates/general_page', $data);
	}
    
	/*
	*
	*	Open the user's wishlist
	*
	*/
	public function wishlist()
	{
		//Required general page data
		$v_data['all_children'] = $this->categories_model->all_child_categories();
		$v_data['parent_categories'] = $this->categories_model->all_parent_categories();
		$v_data['crumbs'] = $this->site_model->get_crumbs();
		
		//page data
		$v_data['all_orders'] = $this->orders_model->get_users_wishlist($this->session->userdata('user_id'));
		$data['content'] = $this->load->view('user/wishlist', $v_data, true);
		
		$data['title'] = $this->site_model->display_page_title();
		$this->load->view('templates/general_page', $data);
	}
	/*
	*
	*	Update a user's account
	*
	*/
	public function create_account()
	{
		//form validation rules
		$this->form_validation->set_rules('last_name', 'Last Names', 'required|xss_clean');
		$this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean');
		$this->form_validation->set_rules('phone', 'Phone', 'required|xss_clean');
		$this->form_validation->set_rules('new_password', 'New Password', 'required|xss_clean');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_userdata('front_error_message', validation_errors());
		}
		
		else
		{
			//check if user has valid login credentials
			if($this->users_model->add_frontend_user())
			{
				$this->session->set_userdata('front_success_message', 'Your details have been successfully submitted. Please login using your email and password');
			}
			
			else
			{
				$this->session->set_userdata('front_error_message', 'Oops something went wrong and we were unable to update your details. Please try again');
			}
		}
		
		redirect('my-account');
	}
    
	/*
	*
	*	Update a user's account
	*
	*/
	public function update_account()
	{
		//form validation rules
		$this->form_validation->set_rules('last_name', 'Last Names', 'required|xss_clean');
		$this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean');
		$this->form_validation->set_rules('phone', 'Phone', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_userdata('account_error_message', validation_errors());
		}
		
		else
		{
			//check if user has valid login credentials
			if($this->users_model->edit_frontend_user($this->session->userdata('member_id')))
			{
				$this->session->set_userdata('account_success_message', 'Your details have been successfully updated');
			}
			
			else
			{
				$this->session->set_userdata('account_error_message', 'Oops something went wrong and we were unable to update your details. Please try again');
			}
		}
		
		redirect('my-account');
	}
    
	/*
	*
	*	Update a user's password
	*
	*/
	public function update_password()
	{
		//form validation rules
		$this->form_validation->set_rules('current_password', 'Current Password', 'required|xss_clean');
		$this->form_validation->set_rules('new_password', 'New Password', 'required|xss_clean');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_userdata('account_error_message', validation_errors());
		}
		
		else
		{
			//update password
			$update = $this->users_model->edit_password($this->session->userdata('member_id'));
			if($update['result'])
			{
				$this->session->set_userdata('account_success_message', 'Your password has been successfully updated');
			}
			
			else
			{
				$this->session->set_userdata('account_error_message', $update['message']);
			}
		}
		
		redirect('my-account');
	}
	public function contact_us()
	{
		# code...
		$v_data['sender_name_error'] = '';
		$v_data['sender_email_error'] = '';
		$v_data['sender_phone_error'] = '';
		$v_data['message_error'] = '';
		
		$v_data['sender_name'] = '';
		$v_data['sender_email'] = '';
		$v_data['sender_phone'] = '';
		$v_data['message'] = '';
		
		//form validation rules
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('sender_name', 'Your Name', 'required');
		$this->form_validation->set_rules('sender_email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('subject', 'Subject', 'xss_clean');
		$this->form_validation->set_rules('message', 'Message', 'required');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			$this->load->model('site/email_model');
			
			$contacts = $this->site_model->get_contacts();
			$message['contacts'] = $contacts;
			if(count($contacts) > 0)
			{
				$email = $contacts['email'];
				$facebook = $contacts['facebook'];
				$linkedin = $contacts['linkedin'];
				$logo = $contacts['logo'];
				$company_name = $contacts['company_name'];
				$phone = $contacts['phone'];
				$address = $contacts['address'];
				$post_code = $contacts['post_code'];
				$city = $contacts['city'];
				$building = $contacts['building'];
				$floor = $contacts['floor'];
				$location = $contacts['location'];
				
				$working_weekday = $contacts['working_weekday'];
				$working_weekend = $contacts['working_weekend'];
			}
			//Notify admin
			$date = date('jS M Y H:i a',strtotime(date('Y-m-d H:i:s')));
			$message['subject'] =  $this->input->post('subject');
			$message['text'] = '<p>member message was sent on '.$date.' saying:</p> 
					<p>'.$this->input->post('message').'</p>
					<p>Their contact details are:</p>
					<p>
						Name: '.$this->input->post('sender_name').'<br/>
						Email: '.$this->input->post('sender_email').'<br/>
						Phone: '.$this->input->post('sender_phone').'<br/>
					</p>';
			$message['text'] = $this->load->view('compose_mail', $message, TRUE);
			
			$sender['email'] = $this->input->post('sender_email');
			$sender['name'] = $this->input->post('sender_name');
			$receiver['email'] = $email;
			// $receiver['email'] = 'marttkip@gmail.com';
			$receiver['name'] = $company_name;
		
			$response = $this->email_model->send_sendgrid_mail_no_attachment($receiver, $sender, $message);
			
			$this->session->set_userdata('account_success_message', 'Your message has been sent successfully. We shall get back to you as soon as possible');
		}
		else
		{
			$validation_errors = validation_errors();
			$this->session->set_userdata('account_error_message', $validation_errors);
			
			//repopulate form data if validation errors are present
			if(!empty($validation_errors))
			{
				//create errors
				$v_data['sender_name_error'] = form_error('sender_name');
				$v_data['sender_email_error'] = form_error('sender_email');
				$v_data['sender_phone_error'] = form_error('sender_phone');
				$v_data['message_error'] = form_error('message');
				
				//repopulate fields
				$v_data['sender_name'] = set_value('sender_name');
				$v_data['sender_email'] = set_value('sender_email');
				$v_data['sender_phone'] = set_value('sender_phone');
				$v_data['message'] = set_value('message');
			}
		}


		redirect('my-account');
	}
}