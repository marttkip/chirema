<?php

class Site_model extends CI_Model 
{
	public function display_page_title()
	{
		$page = explode("/",uri_string());
		$total = count($page);
		$last = $total - 1;
		$name = $this->site_model->decode_web_name($page[$last]);
		
		if(is_numeric($name))
		{
			$last = $last - 1;
			$name = $this->site_model->decode_web_name($page[$last]);
		}
		
		$page_url = ucwords(strtolower($name));
		
		return $page_url;
	}
	
	public function get_crumbs()
	{
		$page = explode("/",uri_string());
		$total = count($page);
		
		$crumb[0]['name'] = ucwords(strtolower($page[0]));
		$crumb[0]['link'] = $page[0];
		
		if($total > 1)
		{
			$sub_page = explode("-",$page[1]);
			$total_sub = count($sub_page);
			$page_name = '';
			
			for($r = 0; $r < $total_sub; $r++)
			{
				$page_name .= ' '.$sub_page[$r];
			}
			$crumb[1]['name'] = ucwords(strtolower($page_name));
			
			if($page[1] == 'category')
			{
				$category_id = $page[2];
				$category_details = $this->categories_model->get_category($category_id);
				
				if($category_details->num_rows() > 0)
				{
					$category = $category_details->row();
					$category_name = $category->category_name;
				}
				
				else
				{
					$category_name = 'No Category';
				}
				
				$crumb[1]['link'] = 'products/all-products/';
				$crumb[2]['name'] = ucwords(strtolower($category_name));
				$crumb[2]['link'] = 'products/category/'.$category_id;
			}
			
			else if($page[1] == 'brand')
			{
				$brand_id = $page[2];
				$brand_details = $this->brands_model->get_brand($brand_id);
				
				if($brand_details->num_rows() > 0)
				{
					$brand = $brand_details->row();
					$brand_name = $brand->brand_name;
				}
				
				else
				{
					$brand_name = 'No Brand';
				}
				
				$crumb[1]['link'] = 'products/all-products/';
				$crumb[2]['name'] = ucwords(strtolower($brand_name));
				$crumb[2]['link'] = 'products/brand/'.$brand_id;
			}
			
			else if($page[1] == 'view-product')
			{
				$product_id = $page[2];
				$product_details = $this->products_model->get_product($product_id);
				
				if($product_details->num_rows() > 0)
				{
					$product = $product_details->row();
					$product_name = $product->product_name;
				}
				
				else
				{
					$product_name = 'No Product';
				}
				
				$crumb[1]['link'] = 'products/all-products/';
				$crumb[2]['name'] = ucwords(strtolower($product_name));
				$crumb[2]['link'] = 'products/view-product/'.$product_id;
			}
			
			else
			{
				$crumb[1]['link'] = '#';
			}
		}
		
		return $crumb;
	}
	
	function generate_price_range()
	{
		$max_price = $this->products_model->get_max_product_price();
		//$min_price = $this->products_model->get_min_product_price();
		
		$interval = $max_price/5;
		
		$range = '';
		$start = 0;
		$end = 0;
		
		for($r = 0; $r < 5; $r++)
		{
			$end = $start + $interval;
			$value = 'KES '.number_format(($start+1), 0, '.', ',').' - KES '.number_format($end, 0, '.', ',');
			$range .= '
			<label class="radio-fancy">
				<input type="radio" name="agree" value="'.$start.'-'.$end.'">
				<span class="light-blue round-corners"><i class="dark-blue round-corners"></i></span>
				<b>'.$value.'</b>
			</label>';
			
			$start = $end;
		}
		
		return $range;
	}
	
	public function get_navigation()
	{
		$page = explode("/",uri_string());
		$total = count($page);
		
		$name = strtolower($page[0]);
		
		$home = '';
		$about = '';
		$shop = '';
		$blog = '';
		$contact = '';
		$security = '';
		$telecommunication = '';
		$networking = '';
		$sell = '';
		
		if($name == 'home')
		{
			$home = 'active';
		}
		
		if($name == 'about')
		{
			$about = 'active';
		}
		
		if($name == 'security')
		{
			$security = 'active';
		}
		if($name == 'telecommunication')
		{
			$telecommunication = 'active';
		}
		if($name == 'networking')
		{
			$networking = 'active';
		}
		
		if($name == 'blog')
		{
			$blog = 'active';
		}
		
		if($name == 'contact')
		{
			$contact = 'active';
		}
		
		if($name == 'sell')
		{
			$sell = 'active';
		}
		
		//variables
		$brands_path = realpath(APPPATH . '../assets/brand/images');
		$categories_path = realpath(APPPATH . '../assets/categories/images');
		
		//image locations
		$brands_location = base_url().'assets/brand/images/';
		$categories_location = base_url().'assets/categories/images/';
		
		//category parents
		$category_parents = $this->categories_model->all_parent_categories();
		$parents = '';
		
		if($category_parents->num_rows() > 0)
		{
			foreach($category_parents->result() as $res)
			{
				$category_id = $res->category_id;
				$category_name = $res->category_name;
				$category_image_name = $res->category_image_name;
				$image = $this->site_model->image_display($categories_path, $categories_location, $category_image_name);
				$category_web_name = $this->site_model->create_web_name($category_name);
				$parents .= '<li class="col-md-4"> <a href="'.site_url().'spareparts/category/'.$category_web_name.'" title="'.$category_name.'" onClick="limit_sub_categories('.$category_id.')"><img src="'.$image.'" alt="'.$category_name.'" class="img-responsice"> <span>'.$category_name.'</span></a></li>';
			}
		}
		
		else
		{
			$parents = '<li>No categories :-(</li>';
		}
		
		//brands
		$active_brands = $this->brands_model->all_active_brands(8);
		$brands = '';
		
		if($active_brands->num_rows() > 0)
		{
			foreach($active_brands->result() as $res)
			{
				$brand_name = $res->brand_name;
				$brand_image_name = $res->brand_image_name;
				$image = $this->site_model->image_display($brands_path, $brands_location, $brand_image_name);
				$brand_web_name = $this->site_model->create_web_name($brand_name);
				$brands .= '<li class="item col-md-3"> <a href="'.site_url().'spareparts/brand/__/'.$brand_web_name.'" title="'.$brand_name.'"><img src="'.$image.'" alt="'.$brand_name.'" class="img-responsice"><br/><div class="brand_name">'.$brand_name.'</div></a></li>';
			}
		}
		
		else
		{
			$brands = '<li>No categories :-(</li>';
		}
		
		
		$category_id = $this->get_category_id('Networking');
		//get departments]
		$resource_query = $this->get_active_child_items($category_id);
		$networking_sub_menu_resources = '';
		if($resource_query->num_rows() > 0)
		{
			foreach($resource_query->result() as $res)
			{
				$category_name = $res->category_name;
				$category_id = $res->category_id;
				$web_name = $this->create_web_name($category_name);
				$networking_sub_menu_resources .= '<li><a href="'.site_url().'networking-search/'.$web_name.'/'.$category_id.'">'.strtoupper($category_name).'</a></li>';
			}
		}
	

		$category_id_two = $this->get_category_id('Telecommunication');
		// var_dump($category_id_two); die();
		//get departments]
		$telecom_query = $this->get_active_child_items($category_id_two);
		$telecommunication_sub_menu_resources = '';
		if($telecom_query->num_rows() > 0)
		{
			foreach($telecom_query->result() as $cat_res)
			{
				$category_name = $cat_res->category_name;
				$category_id = $cat_res->category_id;
				$web_name = $this->create_web_name($category_name);
				$telecommunication_sub_menu_resources .= '<li><a href="'.site_url().'telecommunication-search/'.$web_name.'/'.$category_id.'">'.strtoupper($category_name).'</a></li>';
			}
		}


		$category_id_three = $this->get_category_id('Security');
		//get departments]
		$security_query = $this->get_active_child_items($category_id_three);
		$security_sub_menu_resources = '';
		if($security_query->num_rows() > 0)
		{
			foreach($security_query->result() as $res)
			{
				$category_name = $res->category_name;
				$category_id = $res->category_id;
				$web_name = $this->create_web_name($category_name);
				$security_sub_menu_resources .= '<li><a href="'.site_url().'security-search/'.$web_name.'/'.$category_id.'">'.strtoupper($category_name).'</a></li>';
			}
		}


		$navigation = 
		'
			<li class="'.$home.'"><a href="'.site_url().'home">HOME</a></li>
			<li class="'.$networking.'"><a href="'.site_url().'networking" class="no-wrap">NETWORKING <i class="fa fa-angle-down"></i></a>
				<ul class="ht-dropdown">
					'.$networking_sub_menu_resources.'
				</ul>
			</li>
			<li class="'.$telecommunication.'"><a href="'.site_url().'telecommunication" class="no-wrap">TELECOMMUNICATION <i class="fa fa-angle-down"></i></a>
				<ul class="ht-dropdown">
					'.$telecommunication_sub_menu_resources.'
				</ul>
			</li>
			<li class="'.$security.'"><a href="'.site_url().'security" class="no-wrap">SECURITY <i class="fa fa-angle-down"></i></a>
				<ul class="ht-dropdown">
					'.$security_sub_menu_resources.'
				</ul>
			</li>	
			<li class="'.$about.'"><a href="'.site_url().'about" class="no-wrap">ABOUT US</a></li>	
			<li class="'.$contact.'"><a href="'.site_url().'contact" class="no-wrap">CONTACT US</a></li>
			
		';
		
		return $navigation;
	}
	
	public function get_category_id($category_name,$limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
  		$table = "category";
		$where = "category.category_name = '".$category_name."'";
		
		$this->db->select('category.*');
		$this->db->where($where);
		$this->db->group_by('category_id');
		$this->db->order_by('category_id', 'ASC');
		$query = $this->db->get($table);
		$category_id = 0;
		foreach ($query->result() as $key) {
			# code...
			$category_id = $key->category_id;
		}
		
		return $category_id;
	}
	public function create_web_name($field_name)
	{
		$web_name = str_replace(" ", "-", $field_name);
		
		return $web_name;
	}
	
	public function decode_web_name($web_name)
	{
		$field_name = str_replace("-", " ", $web_name);
		
		return $field_name;
	}
	

	public function get_active_child_items($category_id,$limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
  		$table = "category";
		$where = "category.category_parent = ".$category_id."";
		
		$this->db->select('category.*');
		$this->db->where($where);
		$this->db->group_by('category_name');
		$this->db->order_by('category_id', 'ASC');
		$query = $this->db->get($table);
		
		return $query;
	}

	public function get_active_content_items($product_name, $limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
  		$table = "product,category";
		$where = "product.product_status = 1 AND category.category_id = product.category_id AND product.product_name = '".$product_name."'";
		
		$this->db->select('product.*,category.*');
		$this->db->where($where);
		$this->db->group_by('product_id');
		$this->db->order_by('product_name', 'ASC');
		$query = $this->db->get($table);
		
		return $query;
	}
	
	public function get_active_product_content($product_id, $limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
  		$table = "product,category";
		$where = "product.product_status = 1 AND category.category_id = product.category_id AND product.product_id = '".$product_id."'";
		
		$this->db->select('product.*,category.*');
		$this->db->where($where);
		$this->db->group_by('product_id');
		$this->db->order_by('product_name', 'ASC');
		$query = $this->db->get($table);
		
		return $query;
	}
	public function get_active_product_content_by_category($category_id, $limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
  		$table = "product,category";
		$where = "product.product_status = 1 AND category.category_id = product.category_id AND category.category_id = '".$category_id."'";
		
		$this->db->select('product.*,category.*');
		$this->db->where($where);
		$this->db->group_by('product_id');
		$this->db->order_by('product_name', 'ASC');
		$query = $this->db->get($table);
		
		return $query;
	}	
	public function image_display($base_path, $location, $image_name = NULL)
	{
		$default_image = 'http://placehold.it/300x300&text=Autospares';
		$file_path = $base_path.'/'.$image_name;
		//echo $file_path.'<br/>';
		
		//Check if image was passed
		if($image_name != NULL)
		{
			if(!empty($image_name))
			{
				if((file_exists($file_path)) && ($file_path != $base_path.'\\'))
				{
					return $location.$image_name;
				}
				
				else
				{
					return $default_image;
				}
			}
			
			else
			{
				return $default_image;
			}
		}
		
		else
		{
			return $default_image;
		}
	}
	
	public function get_contacts()
	{
  		$table = "contacts";
		
		$query = $this->db->get($table);
		$contacts = array();
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$contacts['email'] = $row->email;
			$contacts['phone'] = $row->phone;
			$contacts['facebook'] = $row->facebook;
			$contacts['twitter'] = $row->twitter;
			$contacts['linkedin'] = $row->pintrest;
			$contacts['company_name'] = $row->company_name;
			$contacts['logo'] = $row->logo;
			$contacts['address'] = $row->address;
			$contacts['city'] = $row->city;
			$contacts['post_code'] = $row->post_code;
			$contacts['building'] = $row->building;
			$contacts['floor'] = $row->floor;
			$contacts['location'] = $row->location;
			$contacts['working_weekend'] = $row->working_weekend;
			$contacts['working_weekday'] = $row->working_weekday;
			$contacts['mission'] = $row->mission;
			$contacts['vision'] = $row->vision;
			$contacts['motto'] = $row->motto;
			$contacts['about'] = $row->about;
			$contacts['google'] = $row->google;
			$contacts['objectives'] = $row->objectives;
			$contacts['core_values'] = $row->core_values;
		}
		return $contacts;
	}
	
	public function get_breadcrumbs()
	{
		$page = explode("/",uri_string());
		$total = count($page);
		$last = $total - 1;
		$crumbs = '<li><a href="'.site_url().'home">Home </a></li>';
		$name = $this->decode_web_name($page[$last]);
		if(is_numeric($name))
		{
			$total = $total - 1;
		}
		
		for($r = 0; $r < $total; $r++)
		{
			$name = $this->decode_web_name($page[$r]);
			if($r == $last)
			{
				$crumbs .= '<li class="active">'.strtoupper($name).'</li>';
			}
			else
			{
				if($total == 3)
				{
					if($r == 1)
					{
						$crumbs .= '<li><a href="'.site_url().$page[$r-1].'/'.strtolower($name).'">'.strtoupper($name).'</a></li>';
					}
					else
					{
						$crumbs .= '<li><a href="'.site_url().strtolower($name).'">'.strtoupper($name).'</a></li>';
					}
				}
				else
				{
					$crumbs .= '<li><a href="'.site_url().strtolower($name).'">'.strtoupper($name).'</a></li>';
				}
			}
		}
		
		return $crumbs;
	}
	public function submit_query_details()
	{
		$this->load->model('site/email_model');
		$this->load->library('Mandrill', $this->config->item('mandrill_key'));
		$data = array(
			'name'=>ucwords(strtolower($this->input->post('name'))),
			'email'=>$this->input->post('email'),
			'phone'=>$this->input->post('phone'),
			'zip'=>$this->input->post('zip'),
			'query'=>$this->input->post('query'),
			'created'=>date('Y-m-d')
		);			
		if($this->db->insert('enquiry', $data))
		{
			$subject = "".$this->input->post('name')."'s Autospares Query ";
			$message = ' Name :'.$this->input->post('name').' <br>
						 Email : '.$this->input->post('email').' <br>
						 Phone : '.$this->input->post('phone').' <br>

					<p>'.$this->input->post('query').'</p>
					';
			$sender_email = $this->input->post('email');
			$shopping = "";
			$from = $this->input->post('name');
			
			$button = '';
			$button = '';
			 $this->email_model->send_mandrill_mail('marttkip@gmail.com', "Hi Admin", $subject, $message, $sender_email, $shopping, $from, $button);
		
			return TRUE;
		}
		else{
			return FALSE;
		}


	}
	public function send_to_friend($product_id)
	{
		$friends_email = $this->input->post('friends_email');
		$your_email = $this->input->post('your_email');
		$name = $this->input->post('name');
		$friends_email = $this->input->post('friends_email');

		$this->load->model('site/email_model');
		$this->load->library('Mandrill', $this->config->item('mandrill_key'));

		$product_name = $this->products_model->get_product_info($product_id);

		$product_details = $this->products_model->get_product_details($product_id);
		foreach ($product_details->result() as $key) {
			# code...
			$tiny_url = $key->tiny_url;
			$product_code = $key->product_code;
		}

		$subject = " Product Review ";
		$message = '<p> Hello, Please check out this product on '.$tiny_url.'</p>
				';
		$sender_email = $this->input->post('your_email');
		$shopping = "";
		$from = $this->input->post('name');

		$button = '';
		$button = '';
		
		$this->email_model->send_mandrill_mail($friends_email, "Hi Admin", $subject, $message, $sender_email, $shopping, $from, $button);
		return TRUE;
	}

}

?>