<?php
	$cat = $product_details->row();
	//the product details
	$product_code = $cat->product_code;
	$product_name = $cat->product_name;
	$product_year = $cat->product_year;
	$product_id = $cat->product_id;
	$product_description = $cat->product_description;
	$mini_desc = implode(' ', array_slice(explode(' ', $product_description), 0, 10));
	$product_selling_price = number_format($cat->product_selling_price, 0, '.', ',');
	$product_balance = $cat->product_balance;
	$product_status = $cat->product_status;
	$product_image_name = $cat->product_image_name;
	$category_name = $cat->category_name;
	$product_date = date('jS M Y',strtotime($cat->product_date));
	$product_year = $cat->product_year;
	$model = $cat->brand_model_name;
	$brand = $cat->brand_name;
	$location_name = $cat->location_name;
	$customer_name = '';//$cat->customer_name;
	$customer_phone = '';//$cat->customer_phone;
	$customer_email = '';//$cat->customer_email;
	$tiny_url = $cat->tiny_url;
	$prod_name = $brand.' '.$model.' '.$category_name;
	$product_web_name = $this->site_model->create_web_name($product_code.' '.$prod_name);
	$image_parent = $this->site_model->image_display($products_path, $products_location, $product_image_name);
	
	if($product_year == '0000')
	{
		$product_year = '';
	}


	$photos ='';
	$photos_minor ='';
	if($product_images->num_rows() > 0)
	{
		$count = 0;
		foreach($product_images->result() as $prod)
		{
			 $image = $prod->product_image_name;
			 $thumb = $prod->product_image_thumb;
			 $image = $this->site_model->image_display($products_path, $products_location, $image);
			 $thumb = $this->site_model->image_display($products_path, $products_location, $thumb);

			if($count == 0)
			{
				$items = 'show active';
			}
			else
			{
				$items = '';
			}
			$photos .='
						<div id="thumb'.$count.'" class="tab-pane fade show">
                            <a data-fancybox="images" href="'.$image.'"><img src="'.$image.'" alt="product-view"></a>
                        </div>';

			$photos_minor .='<a class="active" data-toggle="tab" href="#thumb'.$count.'"><img src="'.$thumb.'" alt="product-thumbnail"></a>';
			 $count++;
		}
	}

	// var_dump($product_images->num_rows()); die();
?>
<!-- Product Thumbnail Start -->
<div class="main-product-thumbnail ptb-45">
    <div class="container">
        <div class="thumb-bg">
            <div class="row">
                <!-- Main Thumbnail Image Start -->
                <div class="col-lg-5 mb-all-40">
                    <!-- Thumbnail Large Image start -->
                    <div class="tab-content">
                        <div id="thumb1" class="tab-pane fade show active">
                            <a data-fancybox="images" href="<?php echo $image_parent?>"><img src="<?php echo $image_parent?>" alt="product-view"></a>
                        </div>
                        <?php echo $photos;?>
                    </div>
                    <!-- Thumbnail Large Image End -->
                    <!-- Thumbnail Image End -->
                    <div class="product-thumbnail">
                        <div class="thumb-menu owl-carousel nav tabs-area" role="tablist">
                           <?php echo $photos_minor;?>
                        </div>
                    </div>
                    <!-- Thumbnail image end -->
                </div>
                <!-- Main Thumbnail Image End -->
                <!-- Thumbnail Description Start -->
                <div class="col-lg-7">
                    <div class="thubnail-desc fix">
                        <h2 class="product-header"><span class="price"><?php echo strtoupper($product_name.' - '.$product_code);?></span> </h2>
                        <div class="rating-summary fix mtb-10">
                            <div class="rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                            </div>
                        </div>
                       
                        <p class="mb-20 pro-desc-details"><?php echo $product_description;?></p>
                        <div class="socila-sharing mt-25">
                            <ul class="d-flex" data-easyshare data-easyshare-url="">
                                <li>share</li>
                                 	
                                <li><a  data-easyshare-button="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a data-easyshare-button="twitter" data-easyshare-tweet-text=""><i class="fa fa-twitter" aria-hidden="true"></i>  <span data-easyshare-button-count="facebook">0</span></a></li>
                                <li><a data-easyshare-button="google"><i class="fa fa-google-plus-official" aria-hidden="true"></i>  <span data-easyshare-button-count="google">0</span></a></li>
                            </ul>
                        </div>
                       
                    </div>
                </div>
                <!-- Thumbnail Description End -->
            </div>
            <!-- Row End -->
        </div>
    </div>
    <!-- Container End -->
</div>
<?php
	$related_products = '';
	if($similar_products->num_rows() > 0)
	{
		$product = $similar_products->result();
		
		foreach($product as $cat)
		{
			$product_code = $cat->product_code;
			$product_year = $cat->product_year;
			$product_id = $cat->product_id;
			$product_description = $cat->product_description;
			$mini_desc = implode(' ', array_slice(explode(' ', $product_description), 0, 10));
			$product_selling_price = number_format($cat->product_selling_price, 0, '.', ',');
			$product_balance = $cat->product_balance;
			$product_status = $cat->product_status;
			$product_image_name = $cat->product_image_name;
			$category_name = $cat->category_name;
			$product_date = date('jS M Y',strtotime($cat->product_date));
			$product_year = $cat->product_year;
			$model = $cat->brand_model_name;
			$brand = $cat->brand_name;
			$location_name = $cat->location_name;
			$customer_name = $cat->customer_name;
			$customer_phone = $cat->customer_phone;
			$customer_email = $cat->customer_email;
			$product_name = $cat->product_name;
			$tiny_url = $cat->tiny_url;
			$product_web_name = $this->site_model->create_web_name($product_code.' '.$product_name);
			$image = $this->site_model->image_display($products_path, $products_location, $product_image_name);
			
			if($product_year == '0000')
			{
				$product_year = '';
			}
			
			$related_products .=
			'
			 <!-- Single Product Start -->
            <div class="single-product">
                <!-- Product Image Start -->
                <div class="pro-img">
                    <a href="'.base_url().'networking/'.$product_web_name.'">
                        <img class="primary-img" src="'.$image.'" alt="single-product">
                        <img class="secondary-img" src="'.$image.'" alt="single-product">
                    </a>
                </div>
                <!-- Product Image End -->
                <!-- Product Content Start -->
                <div class="pro-content">
                    <div class="pro-info">
                        <h4><span class="price"><a href="'.base_url().'networking/'.$product_web_name.'">'.$product_name.'</a></span></h4>
                        <div class="product-rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </div>
                        <p>'.$product_description.'</p>
                    </div>
                    <div class="pro-actions">
                        <div class="actions-primary">
                            <a href="'.base_url().'networking/'.$product_web_name.'" title="View Product"><i class="fa fa-eye"></i> View product </a>
                        </div>
                        <div class="actions-secondary">
                            <a href="product.html#" data-toggle="modal" data-target="#myModal" title="Quick View"><i class="fa fa-heart-o"></i></a>
                            <a href="product.html" title="Details"><i class="fa fa-signal"></i></a>
                        </div>
                    </div>
                </div>
            </div>
			';
		}
	}
?>

<div class="second-featured-products related-pro pb-45">
    <div class="container">
       <!-- Post Title Start -->
       <div class="post-title">
           <h2><i class="fa fa-trophy" aria-hidden="true"></i>Realted products</h2>
       </div>
       <!-- Post Title End -->
        <!-- New Pro Tow Activation Start -->
        <div class="featured-pro-active owl-carousel">
           	<?php echo $related_products;?>
        </div>
        <!-- New Pro Tow Activation End -->
    </div>
    <!-- Container End -->
</div>
<!-- Realated Products End Here -->