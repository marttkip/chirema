<?php
if($products->num_rows() > 0)
{
	$result = '';
	$list_view = '';
	
    foreach($products->result() as $cat)
	{
        $product_code = $cat->product_code;
        $product_year = $cat->product_year;
        $product_id = $cat->product_id;
        $product_description = $cat->product_description;
		$mini_desc = implode(' ', array_slice(explode(' ', $product_description), 0, 10));
        $product_selling_price = number_format($cat->product_selling_price, 0, '.', ',');
        $product_balance = $cat->product_balance;
        $product_status = $cat->product_status;
        $product_image_name = $cat->product_image_name;
        $category_name = $cat->category_name;
        $product_date = date('jS M Y',strtotime($cat->product_date));
        $product_year = $cat->product_year;
        $model = $cat->brand_model_name;
        $brand = $cat->brand_name;
        $location_name = $cat->location_name;
        $customer_name = '';//$cat->customer_name;
        $customer_phone = '';//$cat->customer_phone;
        $customer_email = '';//$cat->customer_email;
        $product_name = $cat->product_name;
		$tiny_url = $cat->tiny_url;
		$prod_name = $brand.' '.$model.' '.$category_name;
		$product_web_name = $this->site_model->create_web_name($product_code.' '.$product_name);
		$image = $this->site_model->image_display($products_path, $products_location, $product_image_name);
		
		if($product_year == '0000')
		{
			$product_year = '';
		}
		
		$result .= '  <div class="col-lg-4 col-md-4 col-sm-6 col-6">
	                                <div class="single-product">
	                                    <!-- Product Image Start -->
	                                    <div class="pro-img">
	                                        <a href="'.base_url().'networking/'.$product_web_name.'">
	                                            <img class="primary-img" src="'.$image.'" alt="single-product">
	                                            <img class="secondary-img" src="'.$image.'" alt="single-product">
	                                        </a>
	                                    </div>
	                                    <div class="pro-content">
	                                        <div class="pro-info">
	                                            <h4><a href="'.base_url().'networking/'.$product_web_name.'"><span class="price">'.strtoupper($product_name).'</span></a></h4>
	                                            <div class="product-rating">
	                                                <i class="fa fa-star"></i>
	                                                <i class="fa fa-star"></i>
	                                                <i class="fa fa-star"></i>
	                                                <i class="fa fa-star"></i>
	                                                <i class="fa fa-star"></i>
	                                            </div>
	                                            <p>'.$product_description.'</p>
	                                        </div>
	                                        <div class="pro-actions">
	                                            <div class="actions-primary">
                                                    <a href="'.base_url().'networking/'.$product_web_name.'" title="View Product"><i class="fa fa-eye"></i> View product </a>
                                                </div>
	                                             <!--<div class="actions-secondary">
	                                                <a href="#" data-toggle="modal" data-target="#myModal" title="" data-original-title="Quick View"><i class="fa fa-heart-o"></i></a>
	                                                <a href="product.html" title="" data-original-title="Details"><i class="fa fa-signal"></i></a>
	                                            </div>-->
	                                        </div>
	                                    </div>
	                                    <!-- Product Content End -->
	                                </div>
	                            </div>
	';

	$list_view .=' <div class="row single-product">         
                                <!-- Product Image Start -->
                                <div class="col-xl-3 col-lg-4 col-md-4 col-sm-5 col-4">
                                    <div class="pro-img">
                                        <a href="'.base_url().'networking/'.$product_web_name.'">
                                            <img class="primary-img" src="'.$image.'" alt="single-product">
                                            <img class="secondary-img" src="'.$image.'" alt="single-product">
                                        </a>
                                    </div>
                                </div>
                                <!-- Product Image End -->
                                <!-- Product Content Start -->
                                <div class="col-xl-9 col-lg-8 col-md-8 col-sm-7 col-8">
                                    <div class="pro-content">
                                        <h4><a href="'.base_url().'networking/'.$product_web_name.'"><span class="price">'.strtoupper($product_name).'</span></a></h4>
                                        <div class="product-rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <p>'.$product_description.'</p>
                                        <div class="pro-actions">
                                           <div class="actions-primary">
                                                    <a href="'.base_url().'networking/'.$product_web_name.'" title="View Product"><i class="fa fa-eye"></i> View product </a>
                                                </div>
                                              <!--<div class="actions-secondary">
                                                    <a href="#" data-toggle="modal" data-target="#myModal" title="" data-original-title="Quick View"><i class="fa fa-heart-o"></i></a>
                                                    <a href="product.html" title="" data-original-title="Details"><i class="fa fa-signal"></i></a>
                                                </div>-->
                                        </div>
                                    </div>
                                </div>
                                <!-- Product Content End -->
                            </div>';
	}
}

else
{
	$result = 'There are no products :-(';
	$list_view = 'There are no products :-(';
}


$categories = '';
$parent_categories = $this->categories_model->all_parent_categories();
if($parent_categories->num_rows() > 0)
{
	$count = 0;
	$parents = $parent_categories->result();
	
	foreach($parents as $par)
	{
		$count++;
		$category_id = $par->category_id;
		$category_name = $parent_category = $par->category_name;
		$total_products = $this->users_model->count_items('product, category', 'product.category_id = category.category_id AND (category.category_id = '.$category_id.' OR category.category_parent = '.$category_id.')');

		$resource_query = $this->site_model->get_active_child_items($category_id);
		$networking_sub_menu_resources = '';
		if($resource_query->num_rows() > 0)
		{
			foreach($resource_query->result() as $res)
			{
				$category_name = $res->category_name;
				$category_id = $res->category_id;
				$web_name = $this->site_model->create_web_name($category_name);
				$networking_sub_menu_resources .= '<li><a href="'.site_url().strtolower($parent_category).'-search/'.$web_name.'/'.$category_id.'">'.$category_name.'</a></li>';
			}
		}
		$categories .= '<li class="has-sub"><a href="#">'.strtoupper($parent_category).' <i class="fa fa-angle-down pull-right"></i></a> 
							<ul class="category-sub">
                                '.$networking_sub_menu_resources.'
                            </ul>
						</li>';
	}
}
?>
<!-- Shop Page Start -->
<div class="main-shop-page ptb-45">
    <div class="container">
        <!-- Row End -->
        <div class="row">
            <!-- Sidebar Shopping Option Start -->
            <div class="col-lg-3 order-2 order-lg-1">
                <div class="sidebar">
                    <!-- Sidebar Electronics Categorie Start -->
                    <div class="electronics mb-30">
                        <h3 class="sidebar-title e-title">CATEGORIES</h3>
                        <div id="shop-cate-toggle" class="category-menu sidebar-menu sidbar-style">
                            <ul>
                               <?php echo $categories;?>
                            </ul>
                        </div>
                        <!-- category-menu-end -->
                    </div>
                    <!-- Sidebar Electronics Categorie End -->
                
                </div>
            </div>
            <!-- Sidebar Shopping Option End -->
            <!-- Product Categorie List Start -->
            <div class="col-lg-9 order-1 order-lg-2">
                <!-- Grid & List View Start -->
                <div class="grid-list-top border-default universal-padding d-md-flex justify-content-md-between align-items-center mb-10">
                    <div class="grid-list-view  mb-sm-10">
                        <ul class="nav tabs-area d-flex align-items-center">
                            <!-- <li><a data-toggle="tab" href="#grid-view"><i class="fa fa-th"></i></a></li> -->
                            <!-- <li>
                                <a class="active" data-toggle="tab" href="#list-view"><i class="fa fa-list-ul"></i></a>
                            </li> -->

                            <li><span class="grid-item-list">There are <?php echo $total;?> products.</span></li>
                        </ul>
                    </div>
                   
                </div>
                <!-- Grid & List View End -->
                <div class="main-categorie mb-all-40">
                    <!-- Grid & List Main Area End -->
                    <div class="tab-content border-default fix">
                        <div id="grid-view" class="tab-pane fade show active">
                            <div class="row">
                                <?php echo $result;?>
                            </div>
                            <!-- Row End -->
                        </div>
                        <!-- #grid view End -->
                        <div id="list-view" class="tab-pane fade ">
                          	<?php echo $list_view;?>
                        </div>
                        <!-- #list view End -->
                        <!-- Product Pagination Info -->
                        <div class="product-pagination mb-20 pb-15">
                            <span class="grid-item-list">Showing <?php echo $first;?> to <?php echo $last;?> of <?php echo $total;?> results</span>
                        </div>
                        <div class="blog-pagination no-margin-top mt-20 ptb-20">
                            <?php if(isset($links)){echo $links;}?>
                        </div>
                       <!--  <ul class="blog-pagination ptb-20">
                            <li class="active"><a href="shop.html#">1</a></li>
                            <li><a href="shop.html#">2</a></li>
                            <li><a href="shop.html#">3</a></li>
                            <li><a href="shop.html#"><i class="fa fa-angle-right"></i></a></li>
                        </ul> -->
                        <!-- End of .blog-pagination -->
                    </div>
                    <!-- Grid & List Main Area End -->
                </div>
            </div>
            <!-- product Categorie List End -->
        </div>
        <!-- Row End -->
    </div>
    <!-- Container End -->
</div>
<!-- Shop Page End -->