<link href="<?php echo base_url();?>assets/css/jasny-bootstrap.css" rel="stylesheet">
<script src="<?php echo base_url()."assets/themes/jasny/js/jasny-bootstrap.js"?>" type="text/javascript"/></script>	
<script src="<?php echo base_url()."assets/js/site_script.js"?>" type="text/javascript"/></script>	
<?php
if($product_results->num_rows() > 0)
{
	foreach ($product_results->result() as $key => $cat) {
		# code...
			
		$product_code = $cat->product_code;
		$product_id = $cat->product_id;
		$brand_id = $cat->brand_id;
		$category_id = $cat->category_id;
		$product_selling_price = number_format($cat->product_selling_price, 0);
		$product_buying_price = $cat->product_buying_price;
		$product_status = $cat->product_status;
		$default_image_name = $cat->product_image_name;
		$product_date = date('jS M Y H:i a',strtotime($cat->product_date));
		$product_name = $cat->product_name;
		$product_description = $cat->product_description;


	}
}
$product_image = $this->products_model->get_product_gallery($product_id);
$image_result = '';
$count =0;

if($product_image->num_rows() > 0)
{
	

	foreach ($product_image->result() as $key => $value) {
		# code...
		$product_image_id = $value->product_image_id;
		$product_image_name = $value->product_image_name;
		$product_image_thumb = $value->product_image_thumb;


		$count++;
		$image_result .= ' <div class="col-md-4">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                            <img src="'.$product_location.$product_image_name.'" class="img-responsive">
                                        
                                        </div>
                                        <div>
                                            <span class="btn btn-file btn-primary"><span class="fileinput-new">Other image</span><span class="fileinput-exists">Change</span><input type="file" name="gallery'.$count.'"></span>
                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>';
	}
}

if($count == 0)
{
	$image_result .= ' <div class="col-md-4">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                            <img src="$gallery1" class="img-responsive">
                                        
                                        </div>
                                        <div>
                                            <span class="btn btn-file btn-primary"><span class="fileinput-new">Other image</span><span class="fileinput-exists">Change</span><input type="file" name="gallery1"></span>
                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class=" fileinput fileinput-new" data-provides="fileinput">
                                        <div class=" fileinput-preview thumbnail" data-trigger="fileinput">
                                            <img src="$gallery2" class="img-responsive">
                                        
                                        </div>
                                        <div>
                                            <span class="btn btn-file btn-primary"><span class="fileinput-new">Other image</span><span class="fileinput-exists">Change</span><input type="file" name="gallery2"></span>
                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>';
}

else if ($count == 1)
{
	$image_result .= ' 
                                <div class="col-md-4">
                                    <div class=" fileinput fileinput-new" data-provides="fileinput">
                                        <div class=" fileinput-preview thumbnail" data-trigger="fileinput">
                                            <img src="$gallery2" class="img-responsive">
                                        
                                        </div>
                                        <div>
                                            <span class="btn btn-file btn-primary"><span class="fileinput-new">Other image</span><span class="fileinput-exists">Change</span><input type="file" name="gallery2"></span>
                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>';
}


?>

  	<div class="main" role="main">
    	<div id="content" class="content full">
        	<div class="container">
        		<a href="<?php echo site_url().'admin/all-products'?>" class="btn btn-warning pull-right">Back to products</a>
            	<?php
				$success = $this->session->userdata('sell_success');
				if(!empty($success))
				{
					?>
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<div class="alert alert-success">
								<strong>Success! </strong> <?php echo $success; ?>
							</div>
						</div>
					</div>
					<?php 
					$this->session->unset_userdata('sell_success');
				}
				
				$error = $this->session->userdata('error');
				if(!empty($error))
				{
					?>
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<div class="alert alert-success">
								<strong>Error!</strong> <?php echo $error; ?>
							</div>
						</div>
					</div>
					<?php 
					$this->session->unset_userdata('error');
				}
			?>
			<?php
			$error2 = validation_errors(); 
			if(!empty($error2)){
				?>
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="alert alert-danger">
							<strong>Error!</strong> <?php echo validation_errors(); ?>
						</div>
					</div>
				</div>
				<?php 
			}
			?>
				<?php
                $attributes = array('role' => 'form', 'class' => 'form-horizontal add_product');
        
                echo form_open_multipart(site_url()."edit-autopart/".$product_id, $attributes);
                ?>
					<section class="signup-form sm-margint">
						<!-- Regular Signup -->
						<div class="regular-signup">
							<h3>Product details</h3>

		
							
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label class="col-md-11">Product Title</label>
										<div class="col-md-11">
											<input type="text" class="form-control" placeholder="Enter product title" name="product_title" value="<?php echo $product_name;?>">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-11">Price</label>
										<div class="col-md-11">
											<input type="number" class="form-control" placeholder="Enter  price" name="product_selling_price" value="<?php echo $product_selling_price;?>">
										</div>
									</div>
									
								</div>
								<!-- <input type="text" class="form-control" placeholder="Enter  price" name="product_category_id_old" id= "product_category_id_other" value="<?php echo $category_id;?>"> -->
								<div class="col-md-4">
									<div class="form-group">
										<label class="col-md-11">Category level 1</label>
										<div class="col-md-11">
											<select class="form-control selectpicker product_category_children" id="product_category_id" name="product_category_id">
												<?php echo $categories;?>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-11">Category level 2</label>
										<div class="col-md-11" id="product_category_child">
											<select class="form-control selectpicker product_category_sub_children" name="product_category_child">
												<option value="">No level 2</option>
												<?php echo $children;?>
											</select>
										</div>
									</div>
									<div class="form-group" style="display: none">
										<label class="col-md-11">Category level 3</label>
										<div class="col-md-11" id="product_category_sub_child">
											<select class="form-control selectpicker" name="product_category_sub_child">
												<option value="">No level 3</option>
												<?php echo $sub_children;?>
											</select>
										</div>
									</div>
									
								</div>
								
								<div class="col-md-4">
									<div class="form-group">
										<label class="col-md-11">Description</label>
										<div class="col-md-11">
											<textarea class="form-control" placeholder="Enter part description" name="product_description" rows="8"><?php echo $product_description;?></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
                
                	<div class="spacer-40"></div>
					
					<section class="signup-form sm-margint">
                        <!-- Social Signup -->
                        <div class="regular-signup">
                            <h3>Product images</h3>
                            
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="">
                                            <img src="<?php echo $product_location.$default_image_name;?>" class="img-responsive">
                                        </div>
                                        <div>
                                            <span class="btn btn-file btn-primary"><span class="fileinput-new">Default Image</span><span class="fileinput-exists">Change</span><input type="file" name="product_image" value="<?php echo $product_location.$default_image_name;?>"></span>
                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                                        
                              	<?php echo $image_result?>
                                <div class="col-sm-8 col-md-offset-2 input-error" id="gallery_error"></div>
                                <div class="col-sm-8 col-md-offset-2 input-error" id="upload_error_msg"></div>
                            </div>
                        </div>
                        <!-- End sell form -->
					</section>
                
                	<div class="spacer-40"></div>
					
					
                    
                    <div class="row">
                    	<div class="col-md-8 col-md-offset-2">
                          
                            <div class="spacer-20"></div>
                            <input type="submit" class="btn btn-primary btn-lg btn-block" value="Edit product">
                        </div>
                    </div>
                <?php echo form_close();?>
            
                <div class="clearfix"></div>
            </div>
        </div>
   	</div>
    <!-- End Body Content -->