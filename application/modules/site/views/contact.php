
 <!-- Google Map Start -->
<!-- <div class="goole-map pt-45">
    <div class="container"> -->
        <!-- <div id="map" style="height:400px"></div> -->
        <!-- <iframe src="https://www.google.com/maps/d/u/0/embed?mid=18gkZvoIKJ-sqHcXO1QJQqU4Jbxg" width="640" height="480"></iframe> -->
        <!-- <iframe src="https://www.google.com/maps/d/embed?mid=18gkZvoIKJ-sqHcXO1QJQqU4Jbxg"  height="450" frameborder="0" style="border:0" allowfullscreen></iframe> -->
        
        <!-- <iframe src="https://www.google.com/maps/d/embed?mid=18gkZvoIKJ-sqHcXO1QJQqU4Jbxg" width="640" height="480"></iframe> -->
   <!--  </div>
</div> -->
<!-- Google Map End -->


<!-- Contact Email Area Start -->
<div class="contact-area ptb-45">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="mb-20">Contact Us</h3>
                <p class="text-capitalize mb-20">Write to us .</p>
            </div>
        </div>
        <div class="row">
            <!-- <div class="col-md-12"> -->
                <div class="col-lg-4">
                    <div class="col-lg-12">
                    <!-- Head office:5th avenue office suites, ground floor.  Along 5th ngong avenue. P. O. Box 8846-00300 Nairobi. Tel:0707662632- -->
                        <h6>Head Office: 5th Avenue</h6><br/>
                        <span>Office Suites,ground floor. <br/> Along 5th ngong avenue</span><br/>
                        <span>P.O. Box 8846 - 00300 Nairobi </span><br/>
                        <span>Tel: 0707 662 632</span>
                    </div>
                    <br/>
                    <div class="col-md-12">
                        <h6>Branch : Menegai House 1 </h6><br/>
                        <span>Gaberone Lane, River Road </span><br/>
                        <span>P.O. Box 8846 - 00300 Nairobi </span><br/>
                        <span>Tel: 0707 662 630</span>
                    </div>
                     <br/>
                    <div class="col-lg-12">
                        <h6>Branch : Cigma Business Center</h6>
                        <span>Nakuru - Nairobi Highway R/Abt</span><br>
                        <span>P.O. Box 18030 - 0200 Nakuru </span><br>
                        <span>Tel: 0726 892 336</span><br>
                    </div>
                </div>
                <div class="col-lg-8">
                     
                      <?php 
                          // echo form_open($this->uri->uri_string(),array("class" => "form", "role" => "form","id"=>"contact_form"));
                            echo form_open_multipart($this->uri->uri_string(),array("class" => "form", "role" => "form","id"=>"application_form"));
                             $success_message = $this->session->userdata('success_message');
              
                              if(!empty($success_message))
                              {
                                  echo '<div class="alert alert-success"><p>'.$success_message.'</p></div>';
                                  $this->session->unset_userdata('success_message');
                              }
                          ?>
                        <div class="address-wrapper row">
                            <div class="col-md-12">
                                <div class="address-fname">
                                    <input class="form-control" type="text" name="sender_name" placeholder="Name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="address-email">
                                    <input class="form-control" type="email" name="sender_email" placeholder="Email">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="address-web">
                                    <input class="form-control" type="text" name="sender_phone" placeholder="Phone">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="address-subject">
                                    <input class="form-control" type="text" name="subject" placeholder="Subject">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="address-textarea">
                                    <textarea name="message" class="form-control" placeholder="Write your message"></textarea>
                                </div>
                            </div>
                        </div>
                        <p class="form-message"></p>
                        <div class="footer-content mail-content clearfix">
                            <div class="send-email float-md-right">
                                <input value="Submit" class="return-customer-btn" type="submit">
                            </div>
                        </div>
                    <?php echo form_close();?>
                </div>
            <!-- </div> -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7977.634761527309!2d36.823372931302806!3d-1.283435599063903!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182f112a4faa622b%3A0x9a4d0ed994f3ce17!2sChirema+Telecommunications+Ltd!5e0!3m2!1sen!2ske!4v1525328730304" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
       
    </div>
</div>
<!-- Contact Email Area End -->

