<?php
	if(count($contacts) > 0)
	{
		$email = $contacts['email'];
		$facebook = $contacts['facebook'];
		$twitter = $contacts['twitter'];
        $linkedin = $contacts['linkedin'];
        $google = $contacts['google'];
		$logo = $contacts['logo'];
		$company_name = $contacts['company_name'];
		$phone = $contacts['phone'];
	}
?>
 
   <!-- Support Area Start Here -->
    <div class="support-area pb-45">
        <div class="container">
            <div class="col-sm-12">
                <div class="row justify-content-md-between justify-content-sm-start">
                    <div class="single-support d-flex align-items-center">
                        <div class="support-icon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <div class="support-desc">
                            <h6>Mon - Fri</h6>
                            <span>7:00 am - 6:00 pm</span>
                            <h6>Sat & Public Holidays : </h6>
                            <span>8:00 am - 4:00 pm</span>
                        </div>
                    </div>
                    <div class="single-support d-flex align-items-center">
                        <div class="support-icon">
                            <i class="fa fa-phone" ></i>
                        </div>
                        <div class="support-desc">
                            <h6><?php echo $phone;?></h6>
                            <span>Free support line!</span>
                        </div>
                    </div>
                    <div class="single-support d-flex align-items-center">
                        <div class="support-icon">
                           <i class="fa fa-envelope"></i>
                        </div>
                        <div class="support-desc">
                            <h6><?php echo $email;?></h6>
                            <span>Orders Support!</span>
                        </div>
                    </div>
                </div>
                <!-- Row End -->
            </div>
        </div>
        <!-- Container End -->
    </div>
    <!-- Support Area End Here -->
    <!-- Signup Newsletter Start -->
    <div class="newsletter light-blue-bg">
        <div class="container">
            <div class="row">
                 <div class="col-xl-5 col-lg-6">
                     <div class="main-news-desc mb-all-30">
                         <div class="news-desc">
                             <h3>Sign Up For Newsletters</h3>
                             <p>Be the First to Know. Sign up for newsletter today</p>
                         </div>
                     </div>
                 </div>
                 <div class="col-xl-7 col-lg-6">
                     <div class="newsletter-box">
                         <form action="<?php echo site_url().'subscribe-newsletter'?>" method="post">
                              <input class="subscribe" placeholder="your email address" name="email" id="subscribe" type="text">
                              <input class="subscribe" placeholder="your email address" name="uri_string" id="uri_string" type="hidden" value="<?php echo $this->uri->uri_string();?>">
                              <button type="submit" class="submit">subscribe!</button>
                         </form>
                     </div>
                 </div>
            </div>
        </div>
    </div>
      <!-- Slider -->


 <!-- Footer Area Start Here -->
<footer class="white-bg pt-0">
    <!-- Footer Top Start -->
  
    <!-- Footer Top End -->
    <!-- Footer Middle Start -->
  
    <!-- Footer Middle End -->
    <!-- Footer Bottom Start -->
    <div class="footer-bottom off-white-bg ptb-25">
        <div class="container">
             <div class="col-sm-12">
                 <div class="row justify-content-md-between justify-content-center footer-bottom-content">                    
                    <p>Copyright © 2018 All Rights Reserved.</p>
                    <div class="">
                <div class="social-footer-list mt-sm-15">
                    <a href="<?php echo $facebook;?>"><i class="fa fa-facebook"></i></a>
                    <a href="<?php echo $twitter;?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    <a href="<?php echo $google;?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                    <a href="<?php echo $linkedin;?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                </div>
            </div>
                    <div class="footer-bottom-nav mt-sm-15">
                        <nav>
                            <ul class="footer-nav-list">
                                <li><a href="<?php echo site_url().'home'?>">home</a></li>
                                <li><a href="<?php echo site_url().'about'?>">about</a></li>
                                <li><a href="<?php echo site_url().'products'?>">products</a></li>
                                <li><a href="<?php echo site_url().'contact'?>">contact</a></li>
                            </ul>
                        </nav>
                    </div>
                 </div>
                 <!-- Row End -->
             </div>
        </div>
        <!-- Container End -->
    </div>
    <!-- Footer Bottom End -->
</footer>
<!-- Footer Area End Here -->
<!-- Quick View Content Start -->

