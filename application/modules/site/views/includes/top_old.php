   <!-- Start Site Header -->
		<div class="site-header-wrapper">
			<header class="site-header">
				<div class="container sp-cont">
					<div class="site-logo">
						<h1><a href="<?php echo site_url();?>"><img src="<?php echo base_url().'assets/logo/'.$logo;?>" class="img-responsive" alt="<?php echo $company_name;?>"></a></h1>
						<!-- <span class="site-tagline">Buying or Selling,<br>just got easier!</span> -->
					</div>
					<div class="header-right">
						<div class="user-login-panel">
							<a href="#" class="user-login-btn" data-toggle="modal" data-target="#loginModal"><i class="fa fa-user"></i></a>
						</div>
						<div class="topnav dd-menu">
							<ul class="top-navigation sf-menu">
								<!-- <li><a href="<?php echo site_url().'spareparts';?>">Buy</a></li>
								<li><a href="<?php echo site_url().'sell';?>">Sell</a></li> -->
								<!-- <li><a href="<?php echo site_url().'join';?>">Join</a></li> -->
							</ul>
						</div>
					</div>
				</div>
			</header>
			<!-- End Site Header -->
			<div class="navbar">
				<div class="container sp-cont">
					<div class="search-function">
						<span><i class="fa fa-phone"></i> Call us <strong><?php echo $phone;?></strong></span>
					</div>
					<a href="#" class="visible-sm visible-xs" id="menu-toggle"><i class="fa fa-bars"></i></a>
					<!-- Main Navigation -->
					<nav class="main-navigation dd-menu toggle-menu" role="navigation">
						<ul class="sf-menu">
                        	
							<?php echo $this->site_model->get_navigation();?>
							
						</ul>
					</nav> 
				</div>
			</div>
		</div>