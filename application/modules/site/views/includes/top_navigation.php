<?php
	if(count($contacts) > 0)
	{
		$email = $contacts['email'];
		$email2 = $contacts['email'];
		$facebook = $contacts['facebook'];
		$twitter = $contacts['twitter'];
		$linkedin = $contacts['linkedin'];
		$logo = $contacts['logo'];
		$company_name = $contacts['company_name'];
		$phone = $contacts['phone'];
		
		if(!empty($email))
		{
			$email = '<div class="top-number"><p><i class="fa fa-envelope-o"></i> '.$email.'</p></div>';
		}
		
		if(!empty($facebook))
		{
			$twitter = '<li class="pm_tip_static_bottom" title="Twitter"><a href="#" class="fa fa-twitter" target="_blank"></a></li>';
		}
		
		if(!empty($facebook))
		{
			$linkedin = '<li class="pm_tip_static_bottom" title="Linkedin"><a href="#" class="fa fa-linkedin" target="_blank"></a></li>';
		}
		
		if(!empty($facebook))
		{
			$google = '<li class="pm_tip_static_bottom" title="Google Plus"><a href="#" class="fa fa-google-plus" target="_blank"></a></li>';
		}
		
		if(!empty($facebook))
		{
			$facebook = '<li class="pm_tip_static_bottom" title="Facebook"><a href="#" class="fa fa-facebook" target="_blank"></a></li>';
		}
	}
	else
	{
		$email = '';
		$facebook = '';
		$twitter = '';
		$linkedin = '';
		$logo = '';
		$company_name = '';
		$google = '';
	}
?>   
<header>
    <!-- Header Top Start Here -->
           <!--  <div class="header-top light-blue-bg">
                <div class="container">
                    <div class="col-sm-12">
                        <div class="row justify-content-md-between justify-content-center">
                            <div class="header-top-left">
                                
                                
                            </div>
                            <div class="header-top-right">
                                <ul>
                                    <?php 
                                        $member_login_status = $this->session->userdata('member_login_status');
                                        if($member_login_status)
                                        {
                                            ?>
                                             <li><a href="<?php echo site_url().'my-account'?>">My Account</a></li>
                                            <li><a href="<?php echo site_url().'sign-out'?>">Sign Out</a></li>
                                            <?php
                                        }
                                        else
                                        {
                                            ?>
                                            <li><a href="<?php echo site_url().'sign-in'?>">Sign in</a></li>
                                            <?php
                                        }
                                    ?>
                                   
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- Header Top End Here -->
    <!-- Header Middle Start Here -->
    <div class="header-middle light-blue-bg ptb-15">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 col-md-12">
                    <div class="logo mb-all-30">
                        <a href="<?php echo base_url().'home';?>"><img src="<?php echo base_url().'assets/logo/'.$logo;?>" alt="logo-image"></a>
                    </div>
                </div>
                <!-- Categorie Search Box Start Here -->
                <div class="col-lg-6 col-md-12">
                    <div class="categorie-search-box">
                        <form action="<?php echo site_url()."search-product";?>" method="post" >
                            <div class="form-group">
                                <select class="bootstrap-select" name="category_name">
                                    <option value="0">All categories</option>
                                    <?php
                                        // $this->load->model('site/site_model');
                                        $categories = $this->categories_model->all_child_categories();
                                        if($categories->num_rows() > 0)
                                        {
                                            foreach ($categories->result() as $key => $value) {
                                                # code...
                                                $category_name = $value->category_name;
                                                ?>
                                                <option value="<?php echo $category_name;?>"><?php echo $category_name;?></option>
                                                <?php
                                            }
                                        }

                                    ?>
                                </select>
                            </div>
                            <input type="text" name="search_item" placeholder="Enter your search key ... ">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>
                <!-- Categorie Search Box End Here -->
                <!-- Cart Box Start Here -->
              
                <!-- Cart Box End Here -->
            </div>
            <!-- Row End -->
        </div>
        <!-- Container End -->
    </div>
    <!-- Header Middle End Here -->
    <!-- Header Bottom Start Here -->
    <div class="header-bottom dark-blue-bg header-sticky">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-8 col-lg-8 col-md-12 ">
                    <nav class="d-none d-lg-block">
                        <ul class="header-bottom-list d-flex">
                        	<?php echo $this->site_model->get_navigation();?>
                        </ul>
                    </nav>
                    <div class="mobile-menu d-block d-lg-none">
                        <nav>
                            <ul>
                            	<?php echo $this->site_model->get_navigation();?>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 text-right">
                    <section class="header-right col-12 d-flex flex-col">
                        <div class="col-md-6 no-wrap">
                            <a role="button" className="btn btn-info" href="tel:+254707662632" target="_blank" class="phone-link"><i class="fa fa-phone"></i></a>
                            <a role="button" className="btn btn-info" href="https://api.whatsapp.com/send?phone=254726892336" target="_blank" class="phone-link"><i class="fa fa-whatsapp"></i></a>
                            <span class="header-helpline"><?php echo $phone;?></span>
                        </div>
                    </section>
                </div>
            </div>
            <!-- Row End -->
        </div>
        <!-- Container End -->
    </div>
    <!-- Header Bottom End Here -->
    <!-- Mobile Vertical Menu Start Here -->
    <div class="container d-block d-lg-none">
        <div class="vertical-menu mt-30">
            
        </div>
    </div>
    <!-- Mobile Vertical Menu Start End -->
</header>
     
<script>
    // create a set interval that is goint to change the phone number and make it have a blinking effect with different colors
    setInterval(function(){
        var colors = ['#ff6600', '#ffff00', '#00ff00', '#ff0000'];
        var color = colors[Math.floor(Math.random() * colors.length)];
        var phone_numbers = ['+254 707 662 630', '+254 726 892 336', '+254 707 662 632'];
        var phone_number = phone_numbers[Math.floor(Math.random() * phone_numbers.length)];
        $('.header-helpline').html(phone_number);
        $('.header-helpline').css('color', color);
        $('.phone-link').css('color', color);
        $('.phone-link').attr('href', 'tel:'+phone_number);
        $('.whatsapp-link').attr('href', 'https://api.whatsapp.com/send?phone='+phone_number);
    }, 5000);
</script>
		