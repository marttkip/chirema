 <?php
$category_id = $this->site_model->get_category_id('Telecommunication');
//get departments]
$networking_query = $this->site_model->get_active_child_items($category_id);
$networking_sub_menu_resources = '';
$count = 0;
$items_showcase = '';
if($networking_query->num_rows() > 0)
{
	foreach($networking_query->result() as $res)
	{
		$category_name = $res->category_name;
		$category_id = $res->category_id;
		$web_name = $this->site_model->create_web_name($category_name);
		$count++;
		if($count == 1)
		{
			$item = 'active';
			$item_show = 'show active';
		}
		else
		{
			$item  = '';
			$item_show = '';
		}
		$networking_sub_menu_resources .= '<li class="nav-item">
				                                <a class="nav-link '.$item.'" data-toggle="tab" href="#'.$web_name.'">'.$category_name.'</a>
				                            </li>';

		$category_rs = $this->site_model->get_active_product_content_by_category($category_id,12);

		// var_dump($category_rs->num_rows()); die();
		$total_items = $category_rs->num_rows();
		if($category_rs->num_rows() > 0)
		{
			$items_showcase .= '<div id="'.$web_name.'" class="tab-pane fade '.$item_show.'">
									<div class="electronics-pro-active owl-carousel">';
			$counting =0;

			foreach ($category_rs->result() as $key => $value) {
				
				$product_name = $value->product_name;
				$product_code = $value->product_code;
				$product_id = $value->product_id;
				$product_description = $value->product_description;
				$product_selling_price = $value->product_selling_price;
				$product_balance = $value->product_balance;
				$product_status = $value->product_status;
				$product_image_name = $value->product_image_name;
				$category_name = $value->category_name;
				$image = $this->site_model->image_display($products_path, $products_location, $product_image_name);

				$product_web_name = $this->site_model->create_web_name($product_code.' '.$product_name);
				$category_web_name = $this->site_model->create_web_name($category_name);

				
				$display = $counting % 2;

				if($display == 0 AND $counting != 0)
				{
					
					$items_showcase .= '</div>';
				}
				if($display == 0)
				{
					
					$items_showcase .='<div class="double-product">';
				}

				$counting++;
				$items_showcase .= '<div class="single-product">
                                            <div class="pro-img">
                                                <a href="'.base_url().'telecommunication/'.$product_web_name.'">
                                                    <img class="primary-img" src="'.$image.'" alt="single-product">
                                                    <img class="secondary-img" src="'.$image.'" alt="single-product">
                                                </a>
                                            </div>
                                            <div class="pro-content">
                                                <h4><a href="'.base_url().'telecommunication/'.$product_web_name.'"><span class="price">'.strtoupper($product_name).'</span></a></h4>
                                                <div class="product-rating">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </div>
                                                <p>'.$product_description.'</p>
                                                <div class="pro-actions">
                                                    <div class="actions-primary">
                                                        <a href="'.base_url().'telecommunication/'.$product_web_name.'" title="View Product"><i class="fa fa-eye"></i> View product </a>
                                                    </div>
                                                      <!--<div class="actions-secondary">
	                                                <a href="#" data-toggle="modal" data-target="#myModal" title="" data-original-title="Quick View"><i class="fa fa-heart-o"></i></a>
	                                                <a href="product.html" title="" data-original-title="Details"><i class="fa fa-signal"></i></a>
	                                            </div>-->
                                                </div>
                                            </div>
                                        </div>';
                         $display2 = $total_items % 2;
                        if($counting == $total_items AND $display2 != 0)
						{
							
							$items_showcase .= '</div>';
						}
                        
				
			}
			$items_showcase .= '</div></div>';
		}

		
	}
}

?>
 <div class="tv-video pb-45">
        <div class="container">
            <div class="row">
                <!-- Electronics Banner Start -->
                <div class="col-xl-3 col-lg-4 col-md-5">
                    <!-- Post Title Start -->
                    <div class="post-title">
                        <h2><i class="fa fa-list" aria-hidden="true"></i>telecommunication</h2>
                    </div>
                    <!-- Post Title End -->
                    <div class="single-banner">
                        <a href="#"><img src="<?php echo base_url()."assets/themes/volga/";?>img/banner/9.png" alt=""></a>
                    </div>
                </div>
                <!-- Electronics Banner End -->
                <div class="col-xl-9 col-lg-8 col-md-7">
                    <div class="main-product-tab-area"> 
                        <!-- Nav tabs -->
                        <ul class="nav tabs-area" role="tablist">
                          	<?php echo $networking_sub_menu_resources?>
                        </ul>
                        <!-- Tab Contetn Start -->
                        <div class="tab-content">
                        	<?php echo $items_showcase;?>
                        </div>
                        <!-- Tab Content End -->
                    </div>
                    <!-- main-product-tab-area-->
                </div>
            </div>
            <!-- Row End -->
        </div>
        <!-- Container End -->
    </div>