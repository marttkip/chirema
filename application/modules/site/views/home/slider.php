<?php
	
	if(count($contacts) > 0)
	{
		$email = $contacts['email'];
		$facebook = $contacts['facebook'];
		$twitter = $contacts['twitter'];
		
		if(!empty($email))
		{
			$email = '<li class="googleplus"><a href="mailto:'.$email.'"><i class="fa fa-envelope-o"></i></a></li>';
		}
		
		if(!empty($facebook))
		{
			$facebook = '<li class="facebook"><a href="'.$facebook.'" target="_blank"><i class="fa fa-facebook"></i></a></li>';
		}
		
		if(!empty($twitter))
		{
			$twitter = '<li class="twitter"><a href="'.$twitter.'" target="_blank"><i class="fa fa-twitter"></i></a></li>';
		}
	}
	else
	{
		$email = '';
		$facebook = '';
		$twitter = '';
	}

	$categories = '';
	$parent_categories = $this->categories_model->all_parent_categories();
	if($parent_categories->num_rows() > 0)
	{
		$count = 0;
		$parents = $parent_categories->result();
		
		foreach($parents as $par)
		{
			$count++;
			$category_id = $par->category_id;
			$category_name = $par->category_name;
			$total_products = $this->users_model->count_items('product, category', 'product.category_id = category.category_id AND (category.category_id = '.$category_id.' OR category.category_parent = '.$category_id.')');
			$categories .= '<li><a href="'.site_url().'products/category/'.$category_id.'"><span><img src="'.base_url().'assets/themes/volga/img/vertical-menu/6.png" alt="menu-icon"></span> '.$category_name.'</a>
                            </li>';
		}
	}
?>

<div class="main-page-banner ptb-30">
    <div class="container">
        <div class="row">
            <!-- Vertical Menu Start Here -->
            
            <!-- Slider Area Start Here -->
            <div class="col-xl-9 col-lg-8">
                <div class="slider-wrapper theme-default">
                    <!-- Slider Background  Image Start-->
                    <div id="slider" class="nivoSlider">

                        <a href="#"><img src="<?php echo base_url().'assets/images/';?>slider1.JPG" data-thumb="<?php echo base_url().'assets/images/';?>slider1.JPG" alt="" title="#htmlcaption" style="height: 515px !important;"/>
                        </a>
                        <a href="#"><img src="<?php echo base_url().'assets/images/';?>slider2.JPG" data-thumb="<?php echo base_url().'assets/images/';?>slider2.JPG" alt="" title="#htmlcaption2" style="height: 515px !important;" /></a>
                        <a href="#"><img src="<?php echo base_url().'assets/images/';?>slider3.JPG" data-thumb="<?php echo base_url().'assets/images/';?>slider3.JPG" alt="" title="#htmlcaption2" style="height: 515px !important;" /></a>
                        <a href="#"><img src="<?php echo base_url().'assets/images/';?>slider4.JPG" data-thumb="<?php echo base_url().'assets/images/';?>slider4.JPG" alt="" title="#htmlcaption2" style="height: 515px !important;" /></a>
                    </div>
                    <!-- Slider Background  Image Start-->
                    <div class="slider-progress"></div>
                </div>
            </div>
            <!-- Slider Area End Here -->
            <!-- Right Slider Banner Start Here -->
            <div class="col-xl-3 col-lg-12">
                <div class="right-sider-banner">
                	<div class="single-banner">
                     	<div id="overlay">
                     		<div id="text">SECURITY</div>
                     	</div>
                        <a href="<?php echo site_url().'security'?>"><img src="<?php echo base_url()."assets/themes/volga/";?>img/banner/3.png" alt=""></a>
                    </div>
                    <div class="single-banner">
                    	<div id="overlay">
						  <div id="text">NETWORKING</div>
						 </div>
						
                        <a href="<?php echo site_url().'networking'?>"><img src="<?php echo base_url()."assets/themes/volga/";?>img/banner/1.png" alt="">

                        </a>
                    </div>
                    <div class="single-banner">
                     	<div id="overlay">
	                     	<div id="text" >TELECOMMUNICATION</div>
	                     </div>
                        <a href="<?php echo site_url().'telecommunication'?>"><img src="<?php echo base_url()."assets/themes/volga/";?>img/banner/2.png" alt=""></a>
                    </div>
                </div>
            </div>
            <!-- Right Slider Banner End Here -->
        </div>
        <!-- Row End -->
    </div>
    <!-- Container End -->
</div>
		