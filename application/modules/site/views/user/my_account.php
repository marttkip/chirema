<?php
  $member_id = $this->session->userdata('member_id');
	$user = $this->login_model->get_member_detail($member_id);
	$first_name = $user->first_name;
  $last_name = $user->other_names;
  $email = $user->email;
  $phone = $user->phone;
  $password = $user->password;

?>

<!-- Breadcrumb Start -->
<div class="breadcrumb-area mt-10">
    <div class="container">
        <div class="breadcrumb">
           <div class="left"> Welcome back <?php echo $first_name.' '.$last_name;?></div>
           <div class="right"> <a href="<?php echo site_url().'sign-out'?>" class="btn btn-sm btn-success pull-right"> Sign out</a></div>
        </div>
    </div>
    <!-- Container End -->
</div>
<!-- Breadcrumb End -->



<div class="log-in ptb-15">
    <div class="container">
      <?php
       $success_message = $this->session->userdata('account_success_message');
       $error_message = $this->session->userdata('account_error_message');
                    
        if(!empty($success_message))
        {
            echo '<div class="alert alert-success"><p>'.$success_message.'</p></div>';
            $this->session->unset_userdata('success_message');
        }

        if(!empty($error_message))
        {
            echo '<div class="alert alert-danger"><p>'.$error_message.'</p></div>';
            $this->session->unset_userdata('error_message');
        }
      ?>
        <div class="row">
            <!-- New Customer Start -->
            <div class="col-md-6">
                <div class="well mb-sm-30">
                    <div class="new-customer">
                        <h3 class="custom-title">Update your account details</h3>
                        <!-- <p>By creating an account you will be able to shop faster</p> -->
                        <div class="row">
                    <div class="col-sm-12">
                      <?php 
                          echo form_open_multipart(base_url().'site/account/update_account',array("class" => "form", "role" => "form","id"=>"login_form"));
                        ?>
                          <div class="form-group">
                                <label>First name</label>
                                <input type="text" name="first_name" placeholder="First name..." id="input-first-name" class="form-control" value="<?php echo $first_name?>">
                            </div>
                            <div class="form-group">
                                <label>Last name</label>
                                <input type="text" name="last_name" placeholder="Last Name..." id="input-last-name"" class="form-control" value="<?php echo $last_name?>">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" name="email" placeholder="Enter your email address..." id="input-email" class="form-control" value="<?php echo $email?>">
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <input type="text" name="phone" placeholder="Phone" id="input-password" value="<?php echo $phone?>" class="form-control">
                            </div>
                            <input type="submit" value="Edit account" class="return-customer-btn">
                        </form>
                         <?php 
                          echo form_open_multipart(base_url().'site/account/update_password',array("class" => "form-register", "role" => "form","id"=>"login_form"));
                        ?>
                          
                            <fieldset>
                                <legend>Change Your Password</legend>
                                <div class="form-group d-md-flex align-items-md-center">
                                    <label class="control-label col-md-4" for="pwd"><span class="require">*</span>Current Password:</label>
                                    <div class="col-md-8">
                                        <input type="password" class="form-control" id="pwd" name="current_password" placeholder="Password">
                                        <input type="hidden" class="form-control" id="pwd" name="slug" value="<?php echo $password?>" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group d-md-flex align-items-md-center">
                                    <label class="control-label col-md-4" for="pwd"><span class="require">*</span>New Password:</label>
                                    <div class="col-md-8">
                                        <input type="password" class="form-control" id="pwd" name="new_password" placeholder="Password">
                                    </div>
                                </div>
                                <div class="form-group d-md-flex align-items-md-center">
                                    <label class="control-label col-md-4" for="pwd-confirm"><span class="require">*</span>Confirm Password</label>
                                    <div class="col-md-8">
                                        <input type="password" class="form-control" id="pwd-confirm" name="confirm_password" placeholder="Confirm password">
                                    </div>
                                </div>
                                 <input type="submit" value="Change password" class="return-customer-btn">
                            </fieldset>
                           
                        </form>
                    </div>
                </div>

                    </div>

                      <!-- Row End -->
        
        <!-- Row End -->
                </div>
            </div>
            <!-- New Customer End -->
            <!-- Returning Customer Start -->
            <div class="col-md-6">
                <div class="well">
                    <div class="return-customer">
                        <h3 class="mb-10 custom-title">Contact Us </h3>
                        <p class="mb-10"><strong>Send us a message</strong></p>
                          <?php 
                              echo form_open_multipart(base_url().'site/account/contact_us',array("class" => "form-register", "role" => "form","id"=>"login_form"));
                            ?>
                            <input type="hidden" name="sender_email" placeholder="Enter your email address..." id="input-email" class="form-control" value="<?php echo $email;?>">
                             <input type="hidden" name="sender_name" placeholder="Enter your email address..." id="input-email" class="form-control" value="<?php echo $first_name;?> <?php echo $last_name;?>">
                             <input type="hidden" name="sender_phone" placeholder="Enter your email address..." id="input-email" class="form-control" value="<?php echo $phone;?>">
                           
                           <input type="hidden" name="subject" placeholder="" id="input-email" class="form-control" value="<?php echo $first_name;?> Message">
                           

                            <div class="form-group d-md-flex align-items-md-center">
                                    <!-- <label class="control-label col-md-4" for="pwd"><span class="require">*</span>Message:</label> -->
                                    <div class="col-md-12">
                                       
                                        <textarea id="input-message" class="form-control" name="message" ></textarea>
                                    </div>
                                </div>
                            
                            <input type="submit" value="Submit Message" class="return-customer-btn">
                        </form>
                    </div>
                </div>
            </div>
            <!-- Returning Customer End -->
        </div>
        <!-- Row End -->
    </div>
    <!-- Container End -->
</div>
