<?php
    if(count($contacts) > 0)
    {
        $email = $contacts['email'];
        $facebook = $contacts['facebook'];
        $twitter = $contacts['twitter'];
        $logo = $contacts['logo'];
        $company_name = $contacts['company_name'];
        $about = $contacts['about'];
        $phone = $contacts['phone'];
    }
?>

<!-- About Us Start Here -->
 <div class="about-us pt-45 mb-20">
    <!-- <div class="col-lg-12"> -->
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="mb-20 about-title">About our success story</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="sidebar-img sidebar-banner mb-all-30">
                        <img src="<?php echo base_url()."assets/images/";?>slider1.png" alt="single-blog-img">
                    </div>
                </div>
                <div class="col-lg-6" style="background-color: white;padding: 22px;">
                    <div class="about-desc">
                        <p style="text-align: justify;"><?php echo $about;?></p>
                    </div>
                </div>
            </div>
        </div>
    <!-- </div> -->
    <!-- Container End -->
 </div>
<!-- About Us End Here -->

 <!-- Brand Banner Area Start Here -->
   <!--  <div class="main-brand-banner ptb-45">
        <div class="container">
            <div class="brand-banner owl-carousel">
                <div class="single-brand">
                    <a href="index.html#"><img class="img" src="<?php echo base_url()."assets/themes/volga/";?>img/banner/1.jpg" alt="brand-image"></a>
                </div>
                <div class="single-brand">
                    <a href="index.html#"><img src="<?php echo base_url()."assets/themes/volga/";?>img/brand/2.jpg" alt="brand-image"></a>
                </div>
                <div class="single-brand">
                    <a href="index.html#"><img src="<?php echo base_url()."assets/themes/volga/";?>img/brand/3.jpg" alt="brand-image"></a>
                </div>
                <div class="single-brand">
                    <a href="index.html#"><img src="<?php echo base_url()."assets/themes/volga/";?>img/brand/4.jpg" alt="brand-image"></a>
                </div>
                <div class="single-brand">
                    <a href="index.html#"><img src="<?php echo base_url()."assets/themes/volga/";?>img/brand/5.jpg" alt="brand-image"></a>
                </div>
                <div class="single-brand">
                    <a href="index.html#"><img src="<?php echo base_url()."assets/themes/volga/";?>img/brand/6.jpg" alt="brand-image"></a>
                </div>
                <div class="single-brand">
                    <a href="index.html#"><img src="<?php echo base_url()."assets/themes/volga/";?>img/brand/7.jpg" alt="brand-image"></a>
                </div>
            </div>
        </div>
    </div> -->
    <!-- Brand Banner Area End Here -->

<!-- About Us Skills Start Here -->
<!-- <div class="about-skill ptb-45">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
               <h3 class="about-title mb-30">Our skills</h3>
                <div class="skill-progress mb-all-40">
                    <div class="progress">
                        <div class="skill-title">Strategy 79%</div>
                        <div class="progress-bar wow fadeInLeft" data-wow-delay="0.2s" role="progressbar" style="width: 79%; visibility: visible; animation-delay: 0.2s; animation-name: fadeInLeft;">
                        </div>
                    </div>
                    <div class="progress">
                        <div class="skill-title">Marketing 96%</div>
                        <div class="progress-bar wow fadeInLeft" data-wow-delay="0.3s" role="progressbar" style="width: 96%; visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft;">
                        </div>
                    </div>
                    <div class="progress">
                        <div class="skill-title">Wordpress Theme 65%</div>
                        <div class="progress-bar wow fadeInLeft" data-wow-delay="0.4s" role="progressbar" style="width: 65%; visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;">
                        </div>
                    </div>
                    <div class="progress">
                        <div class="skill-title">Shopify Theme 75%</div>
                        <div class="progress-bar wow fadeInLeft" data-wow-delay="0.5s" role="progressbar" style="width: 75%; visibility: visible; animation-delay: 0.5s; animation-name: fadeInLeft;">
                        </div>
                    </div>
                    <div class="progress">
                        <div class="skill-title">UI/UX Design 92%</div>
                        <div class="progress-bar wow fadeInLeft" data-wow-delay="0.6s" role="progressbar" style="width: 89%; visibility: visible; animation-delay: 0.6s; animation-name: fadeInLeft;">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="ht-single-about mb-all-40">
                    <h3 class="about-title mb-30">our experiences</h3>
                    <h5>FUSCE FRINGILLA PORTTITOR IACULI SED QUAM LIBERO, ADIPISCING SED ERAT ID</h5>
                    <p class="mb-15">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eu nisi ac mi malesuada vestibulum. Phasellus tempor nunc eleifend cursus molestie. Mauris lectus arcu, pellentesque at sodales sit amet,</p>
                    <p>Donec ornare mattis suscipit. Praesent fermentum accumsan vulputate. Sed velit nulla, sagittis non erat id, dictum vestibulum ligula. Maecenas sed enim sem.</p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="ht-single-about">
                    <h3 class="about-title mb-30">our works</h3>
                    <div class="ht-about-work">
                        <span>1</span>
                        <div class="ht-work-text">
                            <h5><a href="about.html#">INSTALLATION</a></h5>
                            <p>We do site surveys and also install networking(copper), telecommunication and security products.</p>
                        </div>
                    </div>
                    <div class="ht-about-work">
                        <span>2</span>
                        <div class="ht-work-text">
                            <h5><a href="about.html#">TRANSPORT</a></h5>
                            <p>We offer free transport within Nairobi town for goods purchased in bulk quantities</p>
                        </div>
                    </div>
                 
                </div>
            </div>
        </div>
    </div>
</div> -->