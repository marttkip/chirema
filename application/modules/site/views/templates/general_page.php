<?php 
	
	if(!isset($contacts))
	{
		$contacts = $this->site_model->get_contacts();
	}
	$data['contacts'] = $contacts; 

?>
<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <?php echo $this->load->view('site/includes/header', '', TRUE); ?>
    </head>

	<body>
    	 <!-- Main Wrapper Start Here -->
        <div class="wrapper">
           <!-- Newsletter Popup Start -->
            <input type="hidden" id="base_url" value="<?php echo base_url()?>">
            <input type="hidden" id="site_url" value="<?php echo site_url()?>">
    	
            <!-- Top Navigation -->
            <?php echo $this->load->view('site/includes/top_navigation', $data, TRUE); ?>
            
            <?php echo $content;?>
            
            <?php echo $this->load->view('site/includes/footer', $data, TRUE); ?>
                <!--Start of Tawk.to Script-->
            <script type="text/javascript">
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5b30883aeba8cd3125e3223d/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
            })();
            </script>
            <!--End of Tawk.to Script-->
        </div>
        <script src="<?php echo base_url()."assets/themes/volga/";?>js/vendor/jquery-3.2.1.min.js"></script>
        <!-- Countdown js -->
        <script src="<?php echo base_url()."assets/themes/volga/";?>js/jquery.countdown.min.js"></script>
        <!-- Mobile menu js -->
        <script src="<?php echo base_url()."assets/themes/volga/";?>js/jquery.meanmenu.min.js"></script>
        <!-- ScrollUp js -->
        <script src="<?php echo base_url()."assets/themes/volga/";?>js/jquery.scrollUp.js"></script>
        <!-- Nivo slider js -->
        <script src="<?php echo base_url()."assets/themes/volga/";?>js/jquery.nivo.slider.js"></script>
        <!-- Fancybox js -->
        <script src="<?php echo base_url()."assets/themes/volga/";?>js/jquery.fancybox.min.js"></script>
        <!-- Jquery nice select js -->
        <script src="<?php echo base_url()."assets/themes/volga/";?>js/jquery.nice-select.min.js"></script>
        <!-- Jquery ui price slider js -->
        <script src="<?php echo base_url()."assets/themes/volga/";?>js/jquery-ui.min.js"></script>
        <!-- Owl carousel -->
        <script src="<?php echo base_url()."assets/themes/volga/";?>js/owl.carousel.min.js"></script>
        <!-- Bootstrap popper js -->
        <script src="<?php echo base_url()."assets/themes/volga/";?>js/popper.min.js"></script>
        <!-- Bootstrap js -->
        <script src="<?php echo base_url()."assets/themes/volga/";?>js/bootstrap.min.js"></script>
        <!-- Plugin js -->
        <script src="<?php echo base_url()."assets/themes/volga/";?>js/plugins.js"></script>
        <!-- Main activaion js -->
        <script src="<?php echo base_url()."assets/themes/volga/";?>js/main.js"></script>
    </body>
</html>
