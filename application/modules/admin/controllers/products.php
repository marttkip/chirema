<?php session_start();  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/admin/controllers/admin.php";

class Products extends admin {
	var $products_path;
	var $gallery_path;
	var $features_path;
	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('users_model');
		$this->load->model('products_model');
		$this->load->model('categories_model');
		$this->load->model('brands_model');
		$this->load->model('brand_models_model');
		$this->load->model('file_model');
		
		//path to image directory
		// $this->products_path = realpath(APPPATH . '../assets/images/products/images');
		$this->products_path = realpath(APPPATH . '../assets/products/images');
		$this->gallery_path = realpath(APPPATH . '../assets/images/products/gallery');
		$this->features_path = realpath(APPPATH . '../assets/images/features');
	}
    
	/*
	*
	*	Default action is to show all the products
	*
	*/
	public function index() 
	{
		$where = 'product.location_id = location.location_id';
		$table = 'product,location';
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = base_url().'admin/all-products';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = 3;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->products_model->get_all_products($table, $where, $config["per_page"], $page);
		
	
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('products/all_products', $v_data, true);
		
		$data['title'] = 'All Parts';
		
		$this->load->view('templates/general_admin', $data);
	}
    
	/*
	*
	*	Delete an existing product
	*	@param int $product_id
	*
	*/
	public function delete_product($product_id, $page)
	{
		//delete product image
		$query = $this->products_model->get_product($product_id);
		
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
			$image = $result[0]->product_image_name;
			
			//delete image
			$this->file_model->delete_file($this->products_path."\\".$image);
			//delete thumbnail
			$this->file_model->delete_file($this->products_path."\\thumb_".$image);
		}
		
		//delete gallery images
		$query = $this->products_model->get_gallery_images($product_id);
		
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
			foreach($result as $res)
			{
				$image = $res->product_image_name;
				$thumb = $res->product_image_thumb;
				
				//delete image
				$this->file_model->delete_file($this->gallery_path."\\".$image);
				//delete thumbnail
				$this->file_model->delete_file($this->gallery_path."\\".$thumb);
			}
			
			$this->products_model->delete_gallery_images($product_id);
		}
		
		//delete features
		$query = $this->products_model->get_features($product_id);
		
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
			foreach($result as $res)
			{
				$image = $res->image;
				$thumb = $res->thumb;
				
				//delete image
				$this->file_model->delete_file($this->features_path."\\".$image);
				//delete thumbnail
				$this->file_model->delete_file($this->features_path."\\".$thumb);
			}
			
			$this->products_model->delete_features($product_id);
		}
		
		$this->products_model->delete_product($product_id);
		$this->session->set_userdata('success_message', 'Product has been deleted');
		redirect('admin/all-products/'.$page);
	}
    
	/*
	*
	*	Activate an existing product
	*	@param int $product_id
	*
	*/
	public function activate_product($product_id, $page)
	{
		$this->products_model->activate_product($product_id);
		$this->session->set_userdata('success_message', 'Product activated successfully');
		redirect('admin/all-products/'.$page);
	}
    
	/*
	*
	*	Deactivate an existing product
	*	@param int $product_id
	*
	*/
	public function deactivate_product($product_id, $page)
	{
		$this->products_model->deactivate_product($product_id);
		$this->session->set_userdata('success_message', 'Product disabled successfully');
		redirect('admin/all-products/'.$page);
	}
	
	public function upload_images() 
	{
		$this->load->view('upload');
	}
	
	// Upload & Resize in action
    public function do_upload()
    {
		$this->load->library('upload');
		$this->load->library('image_lib');
		
		$resize['width'] = 600;
		$resize['height'] = 800;
		
		$response = $this->file_model->upload_gallery(1, $this->gallery_path, $resize);
        
		if($response)
		{
		   $this->load->view('upload');
		}
		
		else
		{
		   var_dump($response);
		}
    }
	
	/**
	 * Get all the features of a category
	 * Called when adding a new product
	 *
	 * @param int category_id
	 *
	 * @return object
	 *
	 */
	function get_category_features($category_id)
	{
		$data['features'] = $this->features_model->all_features_by_category($category_id);
		
		echo $this->load->view('products/features', $data, TRUE);
	}
	
	function add_new_feature($category_feature_id)
	{
		$feature_name = $this->input->post('sub_feature_name'.$category_feature_id);
		$feature_quantity = $this->input->post('sub_feature_qty'.$category_feature_id);
		$feature_price = $this->input->post('sub_feature_price'.$category_feature_id);
		
		//upload product's gallery images
		$resize['width'] = 600;
		$resize['height'] = 800;
		
		if(is_uploaded_file($_FILES['feature_image'.$category_feature_id]['tmp_name']))
		{
			$this->load->library('image_lib');
			
			$features_path = $this->features_path;
			/*
				-----------------------------------------------------------------------------------------
				Upload image
				-----------------------------------------------------------------------------------------
			*/
			$response = $this->file_model->upload_single_dir_file($features_path, 'feature_image'.$category_feature_id, $resize);
			if($response['check'])
			{
				$file_name = $response['file_name'];
				$thumb_name = $response['thumb_name'];
				
				$options = $this->products_model->add_new_features($category_feature_id, $feature_name, $feature_quantity, $feature_price, $file_name, $thumb_name);
		
				$return['result'] = 'success';
				$return['result_options'] = $options;
			}
		
			else
			{
				$return['result'] = 'image_fail';
				$return['options'] = $response['error'];
			}
		}
		
		else
		{
			$options = $this->products_model->add_new_features($category_feature_id, $feature_name, $feature_quantity, $feature_price);
		
			$return['result'] = 'success';
			$return['result_options'] = $options;
		}
			
		echo json_encode($return);
	}
	
	function delete_new_feature($category_feature_id, $row)
	{
		$_SESSION['name'.$category_feature_id][$row] = NULL;
		$_SESSION['quantity'.$category_feature_id][$row] = NULL;
		$_SESSION['price'.$category_feature_id][$row] = NULL;
		
		//delete images
		if($_SESSION['image'.$category_feature_id][$row] != 'None')
		{
			$this->file_model->delete_file($this->features_path."\\".$_SESSION['image'.$category_feature_id][$row]);
			$this->file_model->delete_file($this->features_path."\\".$_SESSION['thumb'.$category_feature_id][$row]);
		}
		$_SESSION['image'.$category_feature_id][$row] = NULL;
		$_SESSION['thumb'.$category_feature_id][$row] = NULL;
		
		$feature_values = $this->products_model->fetch_new_category_features($category_feature_id);
		$options = '';
		
		if(isset($feature_values))
		{
			$options .= '
				<table class="table table-condensed table-responsive table-hover table-striped">
					<tr>
						<th></th>
						<th>Sub Feature</th>
						<th>Quantity</th>
						<th>Additional Price</th>
						<th>Image</th>
					</tr>
			'.$feature_values.'</table>
			';
		}
		
		else
		{
			$options .= '<p>You have not added any features</p>';
		}
		echo $options;
	}
	
	function delete_product_feature($product_feature_id)
	{
		$features = $this->products_model->get_product_feature($product_feature_id);
		$feat = $features->row();
		
		$feat_id = $feat->feature_id;
		$image = $feat->image;
		$thumb = $feat->thumb;
		
		//delete images
		if($image != 'None')
		{
			$this->file_model->delete_file($this->features_path."\\".$image);
			$this->file_model->delete_file($this->features_path."\\".$thumb);
		}
		
		if($this->products_model->delete_product_feature($product_feature_id))
		{
			
			$v_data['features'] = $this->products_model->get_features($product_id);
			$v_data['all_features'] = $this->features_model->all_features();
			
			echo $this->load->view('products/edit_features', $v_data, TRUE);
		}
		
		else
		{
			echo 'false';
		}
	}
	
	public function delete_gallery_image($product_image_id, $product_id)
	{
		$this->products_model->delete_gallery_image($product_image_id);
		redirect('edit-product/'.$product_id);
	}
	
	function view_features()
	{
		//session_unset();
		$res = $this->products_model->fetch_new_category_features(1);
		var_dump($_SESSION['image1']);
	}


	function validate_product()
	{
		$this->load->library('site/image_lib');
		$this->load->model('admin/file_model');
		
		//upload image if it has been selected
		$response = $this->products_model->upload_product_image($this->products_path, $this->products_location, 'product_image');
		if($response)
		{
			//$v_data['blog_image_location'] = $this->post_location.$this->session->userdata('blog_file_name');
		}
		
		//upload image 2 if it has been selected
		$response = $this->products_model->upload_product_image($this->products_path, $this->products_location, 'gallery1');
		//var_dump($response);die();
		if($response)
		{
			//$v_data['blog_image_location'] = $this->post_location.$this->session->userdata('blog_file_name');
		}
		
		//upload image 3 if it has been selected
		$response = $this->products_model->upload_product_image($this->products_path, $this->products_location, 'gallery2');
		if($response)
		{
			//$v_data['blog_image_location'] = $this->post_location.$this->session->userdata('blog_file_name');
		}
		
		$this->form_validation->set_rules('location_id', 'Location', 'trim|xss_clean');
		$this->form_validation->set_rules('product_year', 'Product Year', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('product_brand_id', 'Brand', 'trim|numeric|xss_clean');
		$this->form_validation->set_rules('product_model_id', 'Model', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('product_description', 'Description', 'trim|required|xss_clean');
		$this->form_validation->set_rules('product_selling_price', 'Selling Price', 'trim|required|xss_clean');
		$this->form_validation->set_rules('product_category_id', 'Category level 1', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('product_category_child', 'Category level 2', 'trim|xss_clean');
		$this->form_validation->set_rules('product_category_sub_child', 'Category level 3', 'trim|xss_clean');
		$this->form_validation->set_rules('agree', 'Agree to t&c', 'trim|required|xss_clean');
		$this->form_validation->set_rules('user_email', 'Email', 'trim|valid_email|xss_clean');
		$this->form_validation->set_rules('seller_name', 'Names', 'trim|required|xss_clean');
		$this->form_validation->set_rules('user_phone', 'Phone', 'trim|required|xss_clean');
		
		if($this->form_validation->run() == FALSE) 
		{
			$this->sell_parts();
		} 
		
		else 
		{
			//check if upload error is present
			$upload_error = $this->session->userdata('error');
			
			if(!empty($upload_error))
			{
				$this->sell_parts();
				// break;
			}
			
			else
			{
				//get product image name
				$file_name = $this->session->userdata('product_image');
				
				//get customer id
				$customer_id = $this->administration_model->add_customer();
				
				//add product
				$product_id = $this->products_model->add_product_site($file_name, $customer_id);
				
				//create tiny url
				$tiny_url = $this->products_model->getTinyUrl($product_id);
				
				$items = array(
							'tiny_url' => $tiny_url
						);
				$table = "product";
				$this->db->where('product_id', $product_id);
				$this->db->update($table, $items);
				
				//upload product images
				if($product_id > 0)
				{
					$file = $this->session->userdata('gallery1');
					
					if(!empty($file))
					{	
						$table = "product_image";
						$data = array(//get the items from the form
									'product_id' => $product_id,
									'product_image_name' => $file,
									'product_image_thumb' => 'thumbnail_'.$file
								);
						$this->db->insert($table, $data);
					}
					
					$file = $this->session->userdata('gallery2');
					
					if(!empty($file))
					{	
						$table = "product_image";
						$data = array(//get the items from the form
									'product_id' => $product_id,
									'product_image_name' => $file,
									'product_image_thumb' => 'thumbnail_'.$file
								);
						$this->db->insert($table, $data);
					}
				}
				
				//unset sessions
				$this->session->unset_userdata('product_image');
				$this->session->unset_userdata('product_image_thumb');
				
				$this->session->unset_userdata('gallery1');
				$this->session->unset_userdata('gallery1_thumb');
				
				$this->session->unset_userdata('gallery2');
				$this->session->unset_userdata('gallery2_thumb');
				
				$this->session->set_userdata('sell_success', 'Your autopart has been successfully added to Autospares. <a href="'.$tiny_url.'" class="btn btn-success">View autopart</a>');
				$this->sell_parts();
			}
		}
	}
	function sell_parts()
	{
		//product images
		$product_image = $this->session->userdata('product_image');
		$gallery1 = $this->session->userdata('gallery1');
		$gallery2 = $this->session->userdata('gallery2');
		
		if(empty($product_image))
		{
			$product_image = 'http://placehold.it/300x300?text=default';
		}
		
		else
		{
			$product_image = $this->products_location.$product_image;
		}
		
		if(empty($gallery1))
		{
			$gallery1 = 'http://placehold.it/300x300?text=image+2';
		}
		
		else
		{
			$gallery1 = $this->products_location.$gallery1;
		}
		
		if(empty($gallery2))
		{
			$gallery2 = 'http://placehold.it/300x300?text=image+3';
		}
		
		else
		{
			$gallery2 = $this->products_location.$gallery2;
		}
		
		$v_data['product_image'] = $product_image;
		$v_data['gallery1'] = $gallery1;
		$v_data['gallery2'] = $gallery2;
		//Brands & models
		$results = $this->brands_model->all_active_brands();
		
		$count = 0;
		$models = '<option value="0">Any model</option>';
		$brands = '<option value="0">Any brand</option>';
		
		if($results->num_rows() > 0)
		{
			$brand_id = set_value('product_brand_id');
			foreach($results->result() as $res)
			{
				if($brand_id == $res->brand_id)
				{
					$brands .= '<option value="'.$res->brand_id.'" selected>'.$res->brand_name.'</option>';
					
					$table = "brand_model";
					$where = "brand_model_status = 1 AND brand_id = ".$brand_id;
					$this->db->order_by('brand_model_name');
					$this->db->where($where);
					$result2 = $this->db->get($table);
					
					if($result2->num_rows() > 0)
					{
						$model_id = set_value('product_model_id');
						foreach($result2->result() as $res2)
						{
							if($model_id == $res2->brand_model_id)
							{
								$models .= "<option value='".$res2->brand_model_id."' selected>".$res2->brand_model_name."</option>";
							}
							
							else
							{
								$models .= "<option value='".$res2->brand_model_id."'>".$res2->brand_model_name."</option>";
							}
						}
					}
				}
				
				else
				{
					$brands .= '<option value="'.$res->brand_id.'">'.$res->brand_name.'</option>';
				}
			}
		}
		$v_data['brands'] = $brands;
		$v_data['models'] = $models;
		
		//Year from & to
		$year_from = 1980;
		$v_data['year_from'] = "";
		$v_data['year_to'] = "";
		$product_year = set_value('product_year');
		for($r = date("Y"); $r >= $year_from; $r--)
		{
			if($product_year == $r)
			{
				$v_data['year_to'] .= "<option selected>".$r."</option>";
			}
			
			else
			{
				$v_data['year_to'] .= "<option>".$r."</option>";
			}
		}
		
		//Categpries & sub categories
		$results = $this->categories_model->all_parent_categories();
		$categories = "";
		$children = "";
		$sub_children = "";
		$count = 0;
		
		if($results->num_rows() > 0)
		{
			$category_id = set_value('product_category_id');
			$child_id = set_value('product_category_child');
			$sub_child_id = set_value('product_category_sub_child');
			
			foreach($results->result() as $res)
			{
				$count++;
				
				if(($count == 1) || (!empty($child_id)))
				{
					$this->db->where("category_parent = ".$res->category_id);
					$this->db->order_by("category_name");
					
					$result2 = $this->db->get("category");
					
					if($result2->num_rows() > 0)
					{
						$count2 = 0;
						foreach($result2->result() as $res2)
						{
							$count2++;
				
							if(($count2 == 1) || (!empty($sub_child_id)))
							{
								$this->db->where("category_parent = ".$res2->category_id);
								$this->db->order_by("category_name");
								
								$result3 = $this->db->get("category");
								
								if($result3->num_rows() > 0)
								{
									foreach($result3->result() as $res3)
									{
										if($sub_child_id == $res3->category_id)
										{
											$sub_children .= '<option value="'.$res3->category_id.'" selected>'.$res3->category_name.'</option>';
										}
										
										else
										{
											$sub_children .= '<option value="'.$res3->category_id.'">'.$res3->category_name.'</option>';
										}
										
									}
								}
							}
							if($child_id == $res2->category_id)
							{
								$children .= '<option value="'.$res2->category_id.'" selected>'.$res2->category_name.'</option>';
							}
							
							else
							{
								$children .= '<option value="'.$res2->category_id.'">'.$res2->category_name.'</option>';
							}
							
						}
					}
				}
				
				if($category_id == $res->category_id)
				{
					$categories .= '<option value="'.$res->category_id.'" selected>'.$res->category_name.'</option>';
				}
				
				else
				{
					$categories .= '<option value="'.$res->category_id.'">'.$res->category_name.'</option>';
				}
				
			}
		}
		$v_data['categories'] = $categories;
		$v_data['children'] = $children;
		$v_data['sub_children'] = $sub_children;
		
		//Location
		$this->db->order_by('location_name');
		$results = $this->db->get('location');
		$locations = "";
		$location_id = set_value('location_id');
		
		if($results->num_rows() > 0)
		{
			foreach($results->result() as $res)
			{
				if($location_id == $res->location_id)
				{
					$locations .= '<option value="'.$res->location_id.'" selected>'.$res->location_name.'</option>';
				}
				
				else
				{
					$locations .= '<option value="'.$res->location_id.'">'.$res->location_name.'</option>';
				}
			}
		}
		$v_data['locations'] = $locations;
		
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		
		$data['content'] = $this->load->view('products/add_product', $v_data, true);
		
		$this->load->view('templates/general_page', $data);
	}


	function add_product()
	{
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		
		$data['content'] = $this->load->view('admin/products/add_product', $v_data, true);
		
		$this->load->view('templates/general_page', $data);
	}
}
?>